/*
Navicat MySQL Data Transfer

Source Server         : localhost_80
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pinjamlaptop

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-11 10:21:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pl_admin
-- ----------------------------
DROP TABLE IF EXISTS `pl_admin`;
CREATE TABLE `pl_admin` (
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namaadmin` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_admin
-- ----------------------------
INSERT INTO `pl_admin` VALUES ('admin', 'd3afa01c8b33a18a9a360c76233221f6', 'Admin', '2019-12-06 13:56:27', '2019-12-20 15:43:46');

-- ----------------------------
-- Table structure for pl_datapinjam
-- ----------------------------
DROP TABLE IF EXISTS `pl_datapinjam`;
CREATE TABLE `pl_datapinjam` (
  `iddatapinjam` int(10) NOT NULL AUTO_INCREMENT,
  `idsantri` int(10) DEFAULT NULL,
  `tanggalpinjam` datetime DEFAULT NULL,
  `tanggalkembali` datetime DEFAULT NULL,
  `idstatus` int(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`iddatapinjam`),
  KEY `fk_datapinjam_santri` (`idsantri`),
  CONSTRAINT `fk_datapinjam_santri` FOREIGN KEY (`idsantri`) REFERENCES `pl_santri` (`idsantri`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_datapinjam
-- ----------------------------

-- ----------------------------
-- Table structure for pl_kelas
-- ----------------------------
DROP TABLE IF EXISTS `pl_kelas`;
CREATE TABLE `pl_kelas` (
  `idkelas` int(5) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idkelas`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_kelas
-- ----------------------------
INSERT INTO `pl_kelas` VALUES ('1', 'X MIPA', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('2', 'X IPS 1', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('3', 'X IPS 2', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('4', 'XI MIPA', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('5', 'XI IPS', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('6', 'XI IPS 1', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('7', 'XI IPS 2', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('8', 'XII MIPA', '2019-12-20 11:52:10', '2019-12-20 11:52:10');
INSERT INTO `pl_kelas` VALUES ('9', 'XII IPS', '2019-12-20 11:52:10', '2019-12-20 11:52:10');

-- ----------------------------
-- Table structure for pl_konfigurasi
-- ----------------------------
DROP TABLE IF EXISTS `pl_konfigurasi`;
CREATE TABLE `pl_konfigurasi` (
  `idkonfigurasi` int(3) NOT NULL AUTO_INCREMENT,
  `namasistem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notelepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idkonfigurasi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_konfigurasi
-- ----------------------------
INSERT INTO `pl_konfigurasi` VALUES ('1', 'Pinjam Laptop', 'Pesantren IMMIM Putra Makassar', 'Jl. Perintis Kemerdekaan No.KM.10, Tamalanrea Indah, Kec. Tamalanrea, Kota Makassar, Sulawesi Selatan 90245', 'informasi@immim.sch.id', '(0411) 585520', null, null, '2019-12-20 15:59:32', '2019-12-20 15:59:34');

-- ----------------------------
-- Table structure for pl_santri
-- ----------------------------
DROP TABLE IF EXISTS `pl_santri`;
CREATE TABLE `pl_santri` (
  `idsantri` int(10) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lembaga` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idsantri`)
) ENGINE=InnoDB AUTO_INCREMENT=364 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_santri
-- ----------------------------
INSERT INTO `pl_santri` VALUES ('1', '0041857718', 'Muhammad Raffi Hidayat', 'Serang, 21 Juli 2004', 'X MIPA', 'MA', '0041857718', '2019-12-20 00:22:53', '2020-01-02 19:56:55');
INSERT INTO `pl_santri` VALUES ('2', '0050354277', 'Muhammad Ali Husain Ridwan ', 'Balikpapan, 21 Februari 2005', 'X MIPA', 'MA', '0050354277', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('3', '0042379265', 'Moh. Ilham', 'Palu, 10 September 2004', 'X MIPA', 'MA', '0042379265', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('4', '0044250430', 'Muhammad Rafi Shidiq Nurlette', 'Makassar, 03 April 2004', 'X MIPA', 'MA', '0044250430', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('5', '0043602376', 'Abdul Aziz Aminullah ', 'Makassar, 01 Agustus 2004', 'X MIPA', 'MA', '0043602376', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('6', '0046021655', 'Muhammad Fadhil Basri', 'Makassar, 17 Januari 2004', 'X MIPA', 'MA', '0046021655', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('7', '0034486502', 'Ahmad Zubair Mu\'allimuddin', 'Makassar, 10 Oktober 2003', 'X MIPA', 'MA', '0034486502', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('8', '0034464667', 'Muh. Syawal ', 'Makassar, 14 Desember 2003', 'X MIPA', 'MA', '0034464667', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('9', '0030229917', 'Andi Batara Rangga Asri', 'Peneki, 09 September 2003', 'X MIPA', 'MA', '0030229917', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('10', '0035075982', 'Muh. Iqra Ramadhani Jamal', 'Gowa, 14 November 2003', 'X MIPA', 'MA', '0035075982', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('11', '0042916517', 'Yusuf Risaldi ', 'Paselloreng, 09 Mei 2004', 'X MIPA', 'MA', '0042916517', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('12', '0044279178', 'A. Adriansyah ', 'Sinjai, 13 Juli 2004', 'X MIPA', 'MA', '0044279178', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('13', '0044216056', 'Ahmad Fauzi', 'Petoosang, 24 Februari 2004', 'X MIPA', 'MA', '0044216056', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('14', '0055269626', 'Alim Zahal', 'Kendari, 18 Februari 2005', 'X MIPA', 'MA', '0055269626', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('15', '0036547633', 'Rochmat Ziyadatulkhair', 'Makassar, 07 November 2003', 'X MIPA', 'MA', '0036547633', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('16', '0041473381', 'Muh. Arya Syahran', 'Benteng, 02 Januari 2004', 'X MIPA', 'MA', '0041473381', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('17', '0043430557', 'M. Rifai Maulana S.', 'Makassar, 23 Mei 2004', 'X MIPA', 'MA', '0043430557', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('18', '0035794201', 'Raihan Islami Rasya', 'Palu, 22 Desember 2003', 'X MIPA', 'MA', '0035794201', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('19', '0040479514', 'Muh. Akram. Ar', 'Polmas, 23 April 2004', 'X MIPA', 'MA', '0040479514', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('20', '0045910832', 'Muhammad Fadhel Basri', 'Makassar, 17 Januari 2004', 'X MIPA', 'MA', '0045910832', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('21', '0042557682', 'Abdillah Abulkhair', 'Rappang, 19 Mei 2004', 'X MIPA', 'MA', '0042557682', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('22', '0043675067', 'Ahmad Wildan Rahim', 'Soppeng, 14 Januari 2004', 'X MIPA', 'MA', '0043675067', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('23', '0038176901', 'Muh. Ian Raehansyah Enre', 'Makassar, 28 Juni 2003', 'X MIPA', 'MA', '0038176901', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('24', '0034857427', 'Muh. Ihsan Ramadhana', 'Tancung, 06 November 2003', 'X MIPA', 'MA', '0034857427', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('25', '0043090320', 'Nur Ikhlasul Amal', 'Palu, 18 September 2004', 'X MIPA', 'MA', '0043090320', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('26', '0047165864', 'Muhammad Aqsal Amin Latuconsina', 'Palu, 08 Juni 2004', 'X MIPA', 'MA', '0047165864', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('27', '0037112241', 'Muh. Arsyal Rahman', 'Makassar, 25 Oktober 2003', 'X MIPA', 'MA', '0037112241', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('28', '0049087557', 'Muhammad Haniif ', 'Masamba, 14 September 2004', 'X MIPA', 'MA', '0049087557', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('29', '0044650972', 'Raihan Nur Abdillah', 'Makassar, 29 Februari 2004', 'X MIPA', 'MA', '0044650972', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('30', '0037214459', 'Yusuf Al-Qardhawi Al-Mandar', 'Makassar, 14 September 2004', 'X MIPA', 'MA', '0037214459', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('31', '0041578692', 'Andi Aiman Al Ghifary', 'Pare-Pare, 07 Desember 2004', 'X MIPA', 'MA', '0041578692', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('32', '0044029740', 'Andi Fahim Hasan Putrata', 'Makassar, 15 Agustus 2004', 'X MIPA', 'MA', '0044029740', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('33', '0033012580', 'Andi Moh. Riyadh', 'Palu, 22 Mei 2003', 'X MIPA', 'MA', '0033012580', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('34', '0051072808', 'Muh. Zhaky Aprillah', 'Teppo, 20 April 2005', 'X MIPA', 'MA', '0051072808', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('35', '0041578702', 'Muhammad Dzikra Thamisyah Putra', 'Pangkajene, 11 Juni 2004', 'X MIPA', 'MA', '0041578702', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('36', '0045578932', 'Mochammad Darul Fhaiz', 'Makassar, 12 Oktober 2004', 'X MIPA', 'MA', '0045578932', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('37', '0034049691', 'Andi Muhammad Ashabul Qahfi', 'Makassar, 12 September 2003', 'X MIPA', 'MA', '0034049691', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('38', '0046587307', 'Muh. Awal Rif\'at', 'Camba, 01 Januari 2004', 'X IPS 1', 'MA', '0046587307', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('39', '0037585904', 'Muh. Faiq Fauzan', 'Makassar, 12 Oktober 2003', 'X IPS 1', 'MA', '0037585904', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('40', '0059383230', 'Ahmaddin Palamba', 'Makassar, 03 April 2005', 'X IPS 1', 'MA', '0059383230', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('41', '0037470497', 'Muhammad Syafa\'at Muis', 'Tarakan, 20 Agustus 2003', 'X IPS 1', 'MA', '0037470497', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('42', '0041655540', 'Muh. Alif Nur', 'Bulukumba, 01 Januari 2004', 'X IPS 1', 'MA', '0041655540', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('43', '0042736853', 'Syahrul Mubarak', 'Pangkep, 18 Oktober 2004', 'X IPS 1', 'MA', '0042736853', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('44', '0041294606', 'Ahmad Syaguni Majdi', 'Balikpapan, 26 Mei 2004', 'X IPS 1', 'MA', '0041294606', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('45', '0035638074', 'Muh. Fajrin Ramadhan', 'Pangkajene Sidrap, 16 November 2003', 'X IPS 1', 'MA', '0035638074', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('46', '0047262556', 'Muhammad  Fiqram Apriansyah', 'Makassar, 08 April 2004', 'X IPS 1', 'MA', '0047262556', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('47', '0046724797', 'Muhammad Ichsan', 'Toaya, 24 Maret 2004', 'X IPS 1', 'MA', '0046724797', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('48', '0046316622', 'Harisakti Baihaqi', 'Gowa, 01 Oktober 2004', 'X IPS 1', 'MA', '0046316622', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('49', '0051497603', 'Muhammad Adam Al Ghazali', 'Maros, 21 April 2004', 'X IPS 1', 'MA', '0051497603', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('50', '0040537059', 'Raevan Fais Kautsar Amku', 'Makassar, 21 September 2004', 'X IPS 1', 'MA', '0040537059', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('51', '0042837980', 'A. Muh. Rif\'at Syauqi Arifin', 'Makassar, 04 Maret 2004', 'X IPS 1', 'MA', '0042837980', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('52', '0044539480', 'Muh. Ghulam Fayiz Huwaidi', 'Makassar, 26 September 2004', 'X IPS 1', 'MA', '0044539480', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('53', '0044094160', 'Muh. Inal Samali', 'Luwuk, 30 Juni 2004', 'X IPS 1', 'MA', '0044094160', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('54', '0042513612', 'Muhamad Afrizal Pulubuhu', 'Ternate, 14 Maret 2004', 'X IPS 1', 'MA', '0042513612', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('55', '0040394911', 'Achmad Madika Muchsin', 'Nabire, 10 Oktober 2004', 'X IPS 1', 'MA', '0040394911', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('56', '0035013172', 'Imtiaz Muhammad Syihab', 'Palopo, 03 September 2003', 'X IPS 1', 'MA', '0035013172', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('57', '0040577256', 'Muhammad Musvy Fadlan', 'Serui, 10 Juli 2004', 'X IPS 1', 'MA', '0040577256', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('58', '0036591265', 'Muh. Aqil Azhzhahir Subair', 'Palopo, 04 Januari 2004', 'X IPS 2', 'MA', '0036591265', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('59', '0038201509', 'Zulfikar Fahmi', 'Wamena, 21 Desember 2003', 'X IPS 2', 'MA', '0038201509', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('60', '0034911891', 'Adit Rachmat Raehal', 'Sorowako, 29 Desember 2003', 'X IPS 2', 'MA', '0034911891', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('61', '0050455788', 'Muhammad Fauzan Pratama Ilham', 'Jayapura, 21 Januari 2005', 'X IPS 2', 'MA', '0050455788', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('62', '0034802769', 'Mohammad Azwansyah Darwis', 'Tarakan, 23 Juli 2003', 'X IPS 2', 'MA', '0034802769', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('63', '0047460667', 'Rahmat Raihan', 'Makassar, 10 Februari 2004', 'X IPS 2', 'MA', '0047460667', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('64', '0042939943', 'Syahreza Pratama A.', 'Makassar, 28 Maret 2004', 'X IPS 2', 'MA', '0042939943', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('65', '0056651796', 'Muhammad Alwan Khairullah', 'Makassar, 13 April 2004', 'X IPS 2', 'MA', '0056651796', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('66', '0041895401', 'Sukri Jumardin', 'Lawolatu, 28 Mei 2005', 'X IPS 2', 'MA', '0041895401', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('67', '0037172253', 'Andi Muhammad Dzaky Aswan', 'Makassar, 10 November 2003', 'X IPS 2', 'MA', '0037172253', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('68', '0041578618', 'Muhammad Haekal Sunarto', 'Sidrap, 27 April 2004', 'X IPS 2', 'MA', '0041578618', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('69', '0041459066', 'Yusran Dwi Ramadhana', 'Pekalongan, 09 November 2004', 'X IPS 2', 'MA', '0041459066', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('70', '0042779940', 'Ahmad Mujahid', 'Mamuju, 19 September 2004', 'X IPS 2', 'MA', '0042779940', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('71', '0040913936', 'Ahmad Ramadhan', 'Serui, 13 Juni 2004', 'X IPS 2', 'MA', '0040913936', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('72', '0042272219', 'Hairulfikri Kasim', 'Ternate, 28 Oktober 2004', 'X IPS 2', 'MA', '0042272219', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('73', '0049240705', 'Agung Muh. Fathil', 'Talaga I, 25 Maret 2004', 'X IPS 2', 'MA', '0049240705', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('74', '0050413686', 'Muhammad Faqih Rumbaroa', 'Masohi, 06 Februari 2004', 'X IPS 2', 'MA', '0050413686', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('75', '0036306493', 'Andi Asyru Ramadhan Arif', 'Makassar, 04 November 2003', 'X IPS 2', 'MA', '0036306493', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('76', '0046513508', 'Umar M. Hamzah', 'Makassar, 14 Februari 2004', 'X IPS 2', 'MA', '0046513508', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('77', '0048670076', 'Ahmad Dzhaqi Farhan', 'Daruba, 05 Oktober 2003', 'X IPS 2', 'MA', '0048670076', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('78', '0043200055', 'M. Rangga Baasalem', 'Ternate, 25 September 2004', 'X IPS 2', 'MA', '0043200055', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('79', '0022461257', 'Muhammad Handika ', 'Jayapura, 24 Juli 2002', 'X IPS 2', 'MA', '0022461257', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('80', '0030494835', 'Ahmad Rifaldi A.', 'Sungguminasa, 17 Juni 2003', 'XI MIPA', 'MA', '0030494835', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('81', '0034514246', 'Ahlul Quranul', 'Wajo, 08 Juni 2003', 'XI MIPA', 'MA', '0034514246', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('82', '0032752348', 'Muh. Rafli Lambogo', 'Makassar, 20 Januari 2003', 'XI MIPA', 'MA', '0032752348', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('83', '0037680881', 'Muhammad Tegar Tri Nurdinah', 'Makassar, 21 Mei 2003', 'XI MIPA', 'MA', '0037680881', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('84', '0033935182', 'Arham Al Farabi', 'Makassar, 09 Juni 2003', 'XI MIPA', 'MA', '0033935182', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('85', '0045748493', 'Muhammad Gufran Kelsaba', 'Kelaba, 10 Mei 2004', 'XI MIPA', 'MA', '0045748493', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('86', '0034664445', 'A. Putra Mahardika', 'Sinjai, 21 Januari 2003', 'XI MIPA', 'MA', '0034664445', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('87', '0035076471', 'Ahmad Gymnastiar Hamdy', 'Makassar, 19 Januari 2003', 'XI MIPA', 'MA', '0035076471', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('88', '0030935854', 'Fauzan Ananda Syahputra', 'Pangkajene Sidrap, 22 Februari 2003', 'XI MIPA', 'MA', '0030935854', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('89', '0035374995', 'Aa Agiel Tiasa', 'Sungguminasa, 01 Januari 2003', 'XI MIPA', 'MA', '0035374995', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('90', '0039762225', 'Firza Ekaputra Zulfiqar', 'Manado, 04 Maret 2003', 'XI MIPA', 'MA', '0039762225', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('91', '0032694150', 'Muh. Aenun Fakhruddin', 'Makale, 31 Mei 2003', 'XI MIPA', 'MA', '0032694150', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('92', '0031981469', 'Muh. Anugrah Ainul Yaqin', 'Makassar, 22 Mei 2003', 'XI MIPA', 'MA', '0031981469', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('93', '0026683020', 'Muh. Zulfikar', 'Sorowako, 15 Juni 2002', 'XI MIPA', 'MA', '0026683020', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('94', '0024637986', 'Gilang Achmad Syafi\'i', 'Sorowako, 09 Juli 2002', 'XI MIPA', 'MA', '0024637986', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('95', '0039426877', 'Muhammad Fadli Syam', 'Watansoppeng, 20 Maret 2003', 'XI MIPA', 'MA', '0039426877', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('96', '0031040728', 'Muh. Taufiqurrahman', 'Makassar, 14 Maret 2003', 'XI MIPA', 'MA', '0031040728', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('97', '0034240266', 'Muhammad Sahrir', 'Makassar, 15 Februari 2003', 'XI MIPA', 'MA', '0034240266', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('98', '0033213568', 'Ahmad Alkausar', 'Makassar, 26 Agustus 2003', 'XI MIPA', 'MA', '0033213568', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('99', '0031295656', 'Rif\'atul Mahmuda Ridwan', 'Pangkajene Sidrap, 15 Oktober 2003', 'XI MIPA', 'MA', '0031295656', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('100', '0034465986', 'Muh. Albadri Paleppangi', 'Bade, 07 Oktober 2003', 'XI MIPA', 'MA', '0034465986', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('101', '0048028785', 'Muqbilulhady Amirullah', 'Manado, 28 November 2004', 'XI MIPA', 'MA', '0048028785', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('102', '0034459909', 'Andi Amal Tola', 'Makassar, 06 April 2003', 'XI MIPA', 'MA', '0034459909', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('103', '0031138768', 'Rangga Aditya Nugraha', 'Pangkajene Sidrap, 16 Juli 2003', 'XI MIPA', 'MA', '0031138768', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('104', '0037809093', 'Muhammad Jabal Nur ', 'Takalar, 14 September 2003', 'XI MIPA', 'MA', '0037809093', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('105', '0027558209', 'Muhammad Sahih As Siddiq', 'Makassar, 02 Desember 2002', 'XI IPS', 'MA', '0027558209', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('106', '0030511780', 'Muhammad Syahid Agil', 'Ujung Pandang, 11 Agustus 2003', 'XI IPS', 'MA', '0030511780', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('107', '0029783396', 'Alief Habibi Mannan', 'Makassar, 11 November 2002', 'XI IPS', 'MA', '0029783396', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('108', '0031777458', 'Alhar Rifqi Abdillah Alauddin', 'Tembagapura, 25 April 2003', 'XI IPS', 'MA', '0031777458', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('109', '0031830105', 'Muhammad Faturrachman. Sy', 'Pangkajene Sidrap, 20 Februari 2003', 'XI IPS', 'MA', '0031830105', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('110', '0036389861', 'Muhammad Ali Fathir B.', 'Makassar, 07 Agustus 2003', 'XI IPS', 'MA', '0036389861', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('111', '0030432578', 'Almubdi Fathir Muqtadir', 'Jeneponto, 01 Januari 2003', 'XI IPS', 'MA', '0030432578', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('112', '0031812420', 'Ikrar Ammar Andrias Lau', 'Jeneponto, 05 Juli 2003', 'XI IPS', 'MA', '0031812420', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('113', '0030020907', 'Farhan Taufiq', 'Makassar , 03 September 2003', 'XI IPS', 'MA', '0030020907', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('114', '0030456029', 'Andi Muhammad Irsyad Pajalai Manalolo', 'Palopo, 31 Maret 2003', 'XI IPS', 'MA', '0030456029', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('115', '0031457999', 'Rahmad Syafruddin', 'Parepare, 03 Februari 2003', 'XI IPS', 'MA', '0031457999', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('116', '0039488418', 'Muhammad Alif Nur Hidayat Bujan', 'Jayapura, 02 Agustus 2003', 'XI IPS', 'MA', '0039488418', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('117', '0030372695', 'Maulana Multazam', 'Wamena, 13 November 2003', 'XI IPS', 'MA', '0030372695', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('118', '0027701680', 'Muhammad Naufal Ab. Amir', 'Sorong, 30 Oktober 2002', 'XI IPS', 'MA', '0027701680', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('119', '0036862332', 'Muh. Abyan Bahdad', 'Makassar, 30 April 2003', 'XI IPS', 'MA', '0036862332', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('120', '0036701362', 'Muh. Afilla Izaac Madani Mahmud', 'Barru, 02 Oktober 2003', 'XI IPS', 'MA', '0036701362', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('121', '0038072887', 'Muhammad A\'rif Rusdin', 'Belopa, 06 Juni 2003', 'XI IPS', 'MA', '0038072887', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('122', '0037761108', 'Imam Fadhilah', 'Makassar, 24 Juni 2003', 'XI IPS', 'MA', '0037761108', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('123', '0033879342', 'Muh. Khaerul Arifin A.R', 'Makassar, 28 April 2003', 'XI IPS', 'MA', '0033879342', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('124', '0041623424', 'Muh. Dzahwan', 'Jeneponto, 26 Januari 2004', 'XI IPS', 'MA', '0041623424', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('125', '0030354453', 'Muh. Akmal Fuady', 'Gorontalo, 02 Juli 2003', 'XI IPS', 'MA', '0030354453', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('126', '0014808841', 'Ahmad Ramadhany', 'Sorowako, 10 Desember 2001', 'XI IPS', 'MA', '0014808841', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('127', '0025911465', 'Ardika Jendra Hidayah', 'Kendari, 08 September 2002', 'XI IPS', 'MA', '0025911465', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('128', '0033012750', 'Marendra Lampah', 'Palu, 24 Juli 2003', 'XI IPS', 'MA', '0033012750', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('129', '0031695653', 'Rangga Septiawan Gusman', 'Ternate, 09 September 2003', 'XI IPS', 'MA', '0031695653', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('130', '0035996103', 'Andi Ahmad Fathir', 'Bulukumba, 15 Januari 2003', 'XI IPS', 'MA', '0035996103', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('131', '0033653483', 'Muhammad Anugra', 'Makassar, 11 Juli 2003', 'XI IPS', 'MA', '0033653483', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('132', '0039786532', 'Muh. Raafi Alfajar Bj', 'Olo-oloho, 23 Maret 2003', 'XI IPS', 'MA', '0039786532', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('133', '0029321620', 'Ayatollah Ahmad Tovany', 'Sorowako, 16 Juli 2003', 'XI IPS', 'MA', '0029321620', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('134', '0016272928', 'Andi Alfian', 'Atapange, 09 April 2001', 'XII MIPA', 'MA', '0016272928', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('135', '0023879350', 'Ahmad Fauzi Ardana', 'Sidrap, 20 Februari 2000', 'XII MIPA', 'MA', '0023879350', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('136', '0021459715', 'Fatur Rahman Apriliansyah', 'Bone, 30 April 2002', 'XII MIPA', 'MA', '0021459715', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('137', '0029583813', 'Muh. Akram S', 'Bulukumba, 02 Desember 2002', 'XII MIPA', 'MA', '0029583813', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('138', '0025439059', 'M. Bobby Gusti Libran', 'Pangkajene Sidrap, 24 September 2002', 'XII MIPA', 'MA', '0025439059', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('139', '0023491271', 'Muh. Farhan Khaliq', 'Makassar, 28 Juli 2002', 'XII MIPA', 'MA', '0023491271', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('140', '0023695196', 'Salahuddin A.R', 'Makassar, 01 April 2002', 'XII MIPA', 'MA', '0023695196', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('141', '0023017092', 'Achmad Fachri Pangara', 'Leworeng Soppeng, 08 Mei 2002', 'XII MIPA', 'MA', '0023017092', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('142', '0010499350', 'Mustajab', 'Camba, 17 Maret 2001', 'XII MIPA', 'MA', '0010499350', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('143', '0016774573', 'Muhammad Arief Risaldy', 'Makassar, 07 Oktober 2001', 'XII MIPA', 'MA', '0016774573', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('144', '0020658730', 'Muhammad Asy\'min Saleh', 'Gowa, 21 Juli 2002', 'XII MIPA', 'MA', '0020658730', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('145', '0029502499', 'Muhammad Ichsan', 'Makassar, 21 September 2002', 'XII MIPA', 'MA', '0029502499', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('146', '0027648549', 'Muhammad Jamil Zulhaq', 'Parepare, 01 Maret 2002', 'XII MIPA', 'MA', '0027648549', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('147', '0027051221', 'Muhammad Abu Yazid', 'Sugihwaras, 06 Juni 2002', 'XII MIPA', 'MA', '0027051221', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('148', '0027101335', 'Achmad Husein Alqadry', 'Jeneponto, 23 Maret 2002', 'XII MIPA', 'MA', '0027101335', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('149', '0023271187', 'Andi Muhammad Israfil Maulana', 'Makassar, 25 September 2002', 'XII MIPA', 'MA', '0023271187', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('150', '0017006781', 'Ince Fachrul Islam S', 'Pangkajene, 04 Oktober 2001', 'XII MIPA', 'MA', '0017006781', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('151', '0021316427', 'Muhammad Riza Al Ghifari', 'Makassar, 04 Juli 2002', 'XII MIPA', 'MA', '0021316427', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('152', '0028792251', 'Muh. Ivan Sandyna', 'Pinrang, 05 April 2002', 'XII MIPA', 'MA', '0028792251', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('153', '0020081840', 'Ilham Mansis', 'Tasiu, 25 Juni 2002', 'XII MIPA', 'MA', '0020081840', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('154', '0023879349', 'Ahmad Fauza Ardani', 'Sidrap, 20 Februari 2002', 'XII MIPA', 'MA', '0023879349', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('155', '0014531794', 'Nur Muhammad Syafaat Syaharuddin', 'Mamuju, 12 Oktober 2002', 'XII MIPA', 'MA', '0014531794', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('156', '0029807469', 'Taufiq Hidayat', 'Maros, 02 April 2002', 'XII MIPA ', 'MA', '0029807469', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('157', '0023019831', 'Muh. Alwahid Kaldri', 'Makassar, 23 Juli 2002', 'XII MIPA', 'MA', '0023019831', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('158', '0020950782', 'Muhammad Royyan', 'Makassar, 24 April 2002', 'XII MIPA', 'MA', '0020950782', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('159', '0021870791', 'Ahmad Muhaimin Usman', 'Murante, 02 Maret 2002', 'XII IPS', 'MA', '0021870791', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('160', '0025287860', 'Muhammad Farid Hidayat', 'Sorowako, 08 Agustus 2002', 'XII IPS', 'MA', '0025287860', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('161', '0010322419', 'Muhammad Arbihan', 'Makassar, 25 April 2001', 'XII IPS', 'MA', '0010322419', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('162', '0016975369', 'Syahrul Hidayat', 'Makassar, 18 April 2002', 'XII IPS', 'MA', '0016975369', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('163', '0022177534', 'Aria Al Kautsar Gisda', 'Limbung, 09 Oktober 2002', 'XII IPS', 'MA', '0022177534', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('164', '0030214823', 'Farlan Kurniawan', 'Pongo, 07 Januari 2003', 'XII IPS', 'MA', '0030214823', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('165', '0008860045', 'Syahrul', 'Kendari, 16 Agustus 2001', 'XII IPS', 'MA', '0008860045', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('166', '0030130148', 'Arief Nurhidayat', 'Bulukumba, 15 Februari 2003', 'XII IPS', 'MA', '0030130148', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('167', '0017690102', 'Azhar Saaidin Syihab', 'Palopo, 24 September 2001', 'XII IPS', 'MA', '0017690102', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('168', '0023913194', 'Muhammad Arya Takrim', 'Makassar, 30 Agustus 2002', 'XII IPS', 'MA', '0023913194', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('169', '0017536739', 'Muh. Faiz', 'Ujung Pandang, 10 Desember 2001', 'XII IPS', 'MA', '0017536739', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('170', '0027011285', 'Muhammad Syariat Farhat Rizki S', 'Makassar, 10 Februari 2002', 'XII IPS', 'MA', '0027011285', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('171', '0021799667', 'Rychal Deka Pratama A', 'Bau-Bau, 20 Agustus 2002', 'XII IPS', 'MA', '0021799667', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('172', '0023879433', 'Imam Hidayat', 'Pangkajene Sidrap, 18 Mei 2002', 'XII IPS', 'MA', '0023879433', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('173', '0024019757', 'Muhajir', 'Makassar, 27 Agustus 2002', 'XII IPS', 'MA', '0024019757', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('174', '0025107812', 'Andriansyah', 'Sinjai, 27 Juli 2002', 'XII IPS', 'MA', '0025107812', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('175', '0029047749', 'A. Moh. Khatami Ma\'arief', 'Sinjai, 31 Januari 2002', 'XII IPS', 'MA', '0029047749', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('176', '0022905498', 'Ammar Yasir Al-Mujahid', 'Makassar, 06 Agustus 2002', 'XII IPS', 'MA', '0022905498', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('177', '0030272097', 'Muhammad Saddam Latif', 'Kendari, 25 Januari 2003', 'XII IPS', 'MA', '0030272097', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('178', '0024051454', 'Alif Yasin Amin', 'Makassar, 02 Juli 2002', 'XII IPS', 'MA', '0024051454', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('179', '0032701216', 'Sultan Hasanuddin', 'Makassar, 06 Maret 2003', 'XII IPS', 'MA', '0032701216', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('180', '0027517336', 'Faiz Mochammad Noor Ishmat', 'Watampone, 11 April 2002', 'XII IPS', 'MA', '0027517336', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('181', '0022541039', 'Jacher Rizky Hilal Alamsy', 'Manente, 25 Februari 2003', 'XII IPS', 'MA', '0022541039', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('182', '0045458810', 'Andi  Rezky Aryatama Saputra', 'Bulukumba, 13 Agustus 2004', 'X IPS 1', 'SMA', '0045458810', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('183', '0047319652', 'A. Muhammad Rizwaan Rusdy', 'Makassar, 24 Maret 2004', 'X IPS 1', 'SMA', '0047319652', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('184', '0042653516', 'A. Ahmad Faris Fyabyaq Ashar', 'Makassar, 02 Maret 2004', 'X IPS 1', 'SMA', '0042653516', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('185', '0042178560', 'Ahmad Firji Ali Fatah', 'Makassar, 09 September 2004', 'X IPS 1', 'SMA', '0042178560', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('186', '0041069316', 'Alief Muh. Rafi\'i', 'Tompobulu, 03 Mei 2004', 'X IPS 1', 'SMA', '0041069316', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('187', '0043800418', 'Aljilani Septia', 'Makassar, 23 September 2004', 'X IPS 1', 'SMA', '0043800418', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('188', '0043430582', 'Andi Muhammad Adzani Gibran', 'Makassar, 22 Juli 2004', 'X IPS 1', 'SMA', '0043430582', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('189', '0052479016', 'Arief Sabhi Nasir', 'Ambon, 02 April 2005', 'X IPS 1', 'SMA', '0052479016', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('190', '0048839688', 'Awal Furqan', 'Bulukumba, 21 Oktober 2004', 'X IPS 1', 'SMA', '0048839688', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('191', '0049785407', 'Dava Maulana Dzaky', 'Purwakarta, 27 April 2004', 'X IPS 1', 'SMA', '0049785407', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('192', '0041675944', 'Halid Lutfi', 'Makassar, 21 September 2004', 'X IPS 1', 'SMA', '0041675944', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('193', '0041295925', 'Hilalulya', 'Camba, 04 Juni 2004', 'X IPS 1', 'SMA', '0041295925', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('194', '0036438624', 'M. Ryan Ilham', 'Samarinda, 17 Oktober 2003', 'X IPS 1', 'SMA', '0036438624', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('195', '0041603626', 'Muh. Nurfan Sahti M', 'Luwuk, 27 Juni 2004', 'X IPS 1', 'SMA', '0041603626', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('196', '0048022437', 'Muhammad Alif Prabowo', 'Makassar, 25 Maret 2004', 'X IPS 1', 'SMA', '0048022437', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('197', '0046430466', 'Muhammad Padil', 'Berau, 25 April 2004', 'X IPS 1', 'SMA', '0046430466', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('198', '0036728815', 'Muhammad Raihan Syafiq', 'Tarakan, 11 Juni 2003', 'X IPS 1', 'SMA', '0036728815', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('199', '0042412439', 'Muhammad Zahran Ath Toyyib', 'Jayapura, 10 Mei 2004', 'X IPS 1', 'SMA', '0042412439', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('200', '0037157361', 'Naim Nu\'man', 'Sorowako, 28 November 2003', 'X IPS 1', 'SMA', '0037157361', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('201', '0048410249', 'A. Wahyu Amir Pallampa', 'Watampone, 04 April 2004', 'X IPS 2', 'SMA', '0048410249', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('202', '0044519714', 'A. M. Rezky Fauzan Pawennari', 'Makassar, 07 Mei 2004', 'X IPS 2', 'SMA', '0044519714', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('203', '0048200938', 'Ahmad Mulyadi', 'Hila, 23 April 2004', 'X IPS 2', 'SMA', '0048200938', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('204', '0034460900', 'Ajwad Haeqal Munarqa', 'Sorowako, 13 Oktober 2003', 'X IPS 2', 'SMA', '0034460900', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('205', '0035035576', 'Akhmad Mastori', 'Sebatik, 27 Juli 2003', 'X IPS 2', 'SMA', '0035035576', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('206', '9982556342', 'Azka Khaerul Siddiq To Lamanginang Syam', 'Lasusua, 29 Juli 2005', 'X IPS 2', 'SMA', '9982556342', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('207', '0059178178', 'Hadiid Ar Raad', 'Makassar, 26 Januari 2005', 'X IPS 2', 'SMA', '0059178178', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('208', '0046488183', 'Iskandar Agung', 'Bonde, 30 April 2004', 'X IPS 2', 'SMA', '0046488183', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('209', '3043706324', 'Lutfi Fathurrachman', 'Sudu, 08 April 2004', 'X IPS 2', 'SMA', '3043706324', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('210', '0045235169', 'M. Dirga Daffa Zulkifli', 'Barru, 17 Agustus 2004', 'X IPS 2', 'SMA', '0045235169', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('211', '0045347581', 'Mohammad Aqsa', 'Luwuk, 19 Januari 2004', 'X IPS 2', 'SMA', '0045347581', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('212', '0048793758', 'Muh. Fathi Fawwaz Aras', 'Makassar, 03 Januari 2004', 'X IPS 2', 'SMA', '0048793758', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('213', '0037482583', 'Muh. Riza Aditya', 'Palu, 29 Mei 2004', 'X IPS 2', 'SMA', '0037482583', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('214', '0042931058', 'Muhammad Ashabul Kahfi', 'Wahai, 13 Juni 2004', 'X IPS 2', 'SMA', '0042931058', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('215', '0037040208', 'Muhammad Fadil', 'Makassar, 21 Desember 2003', 'X IPS 2', 'SMA', '0037040208', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('216', '0042967985', 'Muhammad Fikri Faiz Zikra', 'Kolaka, 17 Mei 2004', 'X IPS 2', 'SMA', '0042967985', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('217', '0043795061', 'Muhammad Yusuf Putra Idris', 'Makassar, 26 Mei 2004', 'X IPS 2', 'SMA', '0043795061', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('218', '0043800614', 'Rahmat Sobirin Matdoan', 'Fak-Fak, 01 Juli 2004', 'X IPS 2', 'SMA', '0043800614', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('219', '0024606196', 'Reza Maulana Lumintang', 'Manado, 24 Juli 2002', 'X IPS 2', 'SMA', '0024606196', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('220', '0043519996', 'Rifki Syarif', 'Polman, 09 Januari 2004', 'X IPS 2', 'SMA', '0043519996', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('221', '0031902730', 'Ryan Rezki Firmansyah', 'Berau, 24 April 2003', 'X IPS 2', 'SMA', '0031902730', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('222', '0048334822', 'Muh. Rifky Bidarishandy', 'Tolai, 21 Mei 3812', 'X IPS 2', 'SMA', '0048334822', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('223', '0029944815', 'Abdullah Hatami', 'Botomanai, 15 Desember 2002', 'X MIPA', 'SMA', '0029944815', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('224', '0040399536', 'Ahmad Dzaky', 'Makassar, 19 Juni 2004', 'X MIPA', 'SMA', '0040399536', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('225', '0041598765', 'Alwy Shiyam Daud', 'Ternate, 19 November 2004', 'X MIPA', 'SMA', '0041598765', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('226', '0045137081', 'Andi Ahmad Adam Kenni', 'Makassar, 31 Desember 2004', 'X MIPA', 'SMA', '0045137081', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('227', '0037157350', 'Andi Baso Batara', 'Soroako, 01 Oktober 2003', 'X MIPA', 'SMA', '0037157350', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('228', '0043335655', 'Andi Fahmi Husein Putrata', 'Makassar, 15 Agustus 2004', 'X MIPA', 'SMA', '0043335655', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('229', '0043250895', 'Andi Syauqi As\'ad Rabbani', 'Samarinda, 26 April 2004', 'X MIPA', 'SMA', '0043250895', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('230', '0041438745', 'Chikal Aditya Budiman', 'Makassar, 14 Januari 2004', 'X MIPA', 'SMA', '0041438745', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('231', '0036494388', 'Fahmi Firman Syeh', 'Bulukumba, 13 Mei 2004', 'X MIPA', 'SMA', '0048747824', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('232', '0041468043', 'Fatwa Kilian', 'Bas, 10 Juli 2004', 'X MIPA', 'SMA', '0041468043', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('233', '0030373065', 'Hardiman', 'Wamena, 29 Agustus 2003', 'X MIPA', 'SMA', '0030373065', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('234', '0036494388', 'Ismail Malik', 'Pekkabata, 28 Desember 2003', 'X MIPA', 'SMA', '0036494388', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('235', '0048292903', 'Khairul Rajul', 'Makassar, 13 Mei 2004', 'X MIPA', 'SMA', '0048292903', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('236', '0049203937', 'Kurniawan Mursalim', 'Maros, 30 Juli 2004', 'X MIPA', 'SMA', '0049203937', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('237', '0044252457', 'M. Yusuf Zahir', 'Makassar, 09 Agustus 2004', 'X MIPA', 'SMA', '0044252457', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('238', '0049823819', 'Muh. Adriansyah Ansar', 'Makassar, 21 November 2004', 'X MIPA', 'SMA', '0049823819', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('239', '0043010491', 'Muh. Aliy Faiz Al Giffari', 'Bulukumba, 21 Desember 2004', 'X MIPA', 'SMA', '0043010491', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('240', '0036788015', 'Muh. Anis Khairy Sutopo', 'Gowa, 27 September 2003', 'X MIPA', 'SMA', '0036788015', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('241', '0033020938', 'Muh. Jusuf Al Munawara Abustan', 'Biak, 09 Desember 2003', 'X MIPA', 'SMA', '0033020938', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('242', '0049772147', 'Muh. Zaki M Amir Syam', 'Palopo, 23 Agustus 2004', 'X MIPA', 'SMA', '0049772147', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('243', '0043517155', 'Muhammad Adithya Madhani Saifuddin', 'Ternate, 18 Juni 2004', 'X MIPA', 'SMA', '0043517155', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('244', '0035645576', 'Muhammad Agung Syaifullah', 'Makassar, 02 Agustus 2003', 'X MIPA', 'SMA', '0035645576', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('245', '0044175244', 'Muhammad Ali Karim', 'Makassar, 07 Maret 2004', 'X MIPA', 'SMA', '0044175244', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('246', '0038008865', 'Muhammad Bintang Ramadani', 'Makassar, 04 Desember 2003', 'X MIPA', 'SMA', '0038008865', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('247', '0041041900', 'Muhammad Fa\'iz Baso Saleh', 'Makassar, 24 April 2004', 'X MIPA', 'SMA', '0041041900', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('248', '0039436453', 'Muhammad Fadhil', 'Makassar, 12 Agustus 2003', 'X MIPA', 'SMA', '0039436453', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('249', '0055372827', 'Muhammad Fathi Farhan', 'Bima, 26 November 2005', 'X MIPA', 'SMA', '0055372827', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('250', '0055357435', 'Muhammad Furqaan', 'Rappang, 18 Maret 2005', 'X MIPA', 'SMA', '0055357435', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('251', '0047886362', 'Muhammad Iqra Tantu', 'Mamuju, 28 Januari 2005', 'X MIPA', 'SMA', '0047886362', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('252', '0043086246', 'Muhammad King Defano Arfah', 'Sorowako, 27 Februari 2004', 'X MIPA', 'SMA', '0043086246', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('253', '0043654643', 'Muhammad Nur Bashirah', 'Maumere, 19 November 2004', 'X MIPA', 'SMA', '0043654643', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('254', '0044334931', 'Nur Faiq', 'Parepare, 03 Juni 2004', 'X MIPA', 'SMA', '0044334931', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('255', '0041013958', 'Nur Ilham Hidayat', 'Bulukumba, 18 Maret 2004', 'X MIPA', 'SMA', '0041013958', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('256', '0041047345', 'Quwais Ridho', 'Palu, 11 Maret 2004', 'X MIPA', 'SMA', '0041047345', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('257', '0047596473', 'Raihan Muhammad Salomba', 'Bajo, 05 April 2004', 'X MIPA', 'SMA', '0047596473', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('258', '0032320605', 'Roahmat', 'Tarakan, 12 Januari 2003', 'X MIPA', 'SMA', '0032320605', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('259', '0048432947', 'Tholib Ariansa Sabir', 'Polewali, 07 Juli 2004', 'X MIPA', 'SMA', '0048432947', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('260', '0037928361', 'A. Muhammad Ryan Akbar Indra Narasraya', 'Watampone, 26 Agustus 2003', 'XI IPS 1', 'SMA', '0037928361', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('261', '0028231212', 'Abd. Mursyid', 'Masamba, 21 Agustus 2002', 'XI IPS 1', 'SMA', '0028231212', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('262', '0034371549', 'Ahmad Muhaiminul Haq', 'Pangali-Ali, 09 Mei 2003', 'XI IPS 1', 'SMA', '0034371549', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('263', '0033233682', 'Alryanza', 'Sorong, 01 November 2003', 'XI IPS 1', 'SMA', '0033233682', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('264', '0032919945', 'Andi Muhammad Aliefsyah Arianto', 'Makassar, 23 Maret 2003', 'XI IPS 1', 'SMA', '0032919945', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('265', '0038489815', 'Andi Muhammad Ariqsyah Arianto', 'Makassar, 23 Maret 2003', 'XI IPS 1', 'SMA', '0038489815', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('266', '0025287094', 'Andi Muhammad Sahrul Nur', 'Jayapura, 08 Juli 2003', 'XI IPS 1', 'SMA', '0025287094', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('267', '3039223769', 'Andy Muh. Rezky Reynanda Yahya', 'Limboto, 27 Juni 2003', 'XI IPS 1', 'SMA', '3039223769', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('268', '0032945922', 'Arjuna Ismar Sugara', 'Sorowako, 29 Mei 2003', 'XI IPS 1', 'SMA', '0032945922', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('269', '3036566605', 'Brillian Prita Nyala', 'Cakke, 13 Februari 2003', 'XI IPS 1', 'SMA', '3036566605', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('270', '0025039236', 'Faris Panca Julian', 'Manokwari, 10 Juli 2003', 'XI IPS 1', 'SMA', '0025039236', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('271', '0032839798', 'Fathul Mujahid', 'Larompong, 04 Agustus 2003', 'XI IPS 1', 'SMA', '0032839798', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('272', '0034694142', 'Ikhsan Atmaja Tri Widagdo', 'Sorowako, 06 Juni 2003', 'XI IPS 1', 'SMA', '0034694142', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('273', '0029995225', 'Jaenaldi', 'Barong Tongkok, 26 Maret 2001', 'XI IPS 1', 'SMA', '0029995225', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('274', '0037404285', 'Khaerul Ummah', 'Camba, 29 November 2003', 'XI IPS 1', 'SMA', '0037404285', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('275', '0039122380', 'Muh. Anugrah Akbar', 'Bulukumba, 04 Juli 2003', 'XI IPS 1', 'SMA', '0039122380', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('276', '0031507473', 'Muh. Arya Arafah', 'Sinjai, 11 Februari 2003', 'XI IPS 1', 'SMA', '0031507473', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('277', '3039234206', 'Muh. Faridsyah', 'Depok, 03 Januari 2003', 'XI IPS 1', 'SMA', '3039234206', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('278', '0030936312', 'Muhammad Fikrul Ma\'arif Saleh', 'Makassar, 27 Mei 2003', 'XI IPS 1', 'SMA', '0030936312', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('279', '0027385241', 'Muhammad Israr', 'Pare-Pare, 14 Maret 2002', 'XI IPS 1', 'SMA', '0027385241', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('280', '0033136871', 'Muhammad Ziaul Haq', 'Makassar, 04 Februari 2003', 'XI IPS 1', 'SMA', '0033136871', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('281', '0038557589', 'Nur Halim Fakhri', 'Makassar, 04 Maret 2003', 'XI IPS 1', 'SMA', '0038557589', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('282', '0020088510', 'Rahmat Setiawan', 'Ujung Pandang, 31 Oktober 2002', 'XI IPS 1', 'SMA', '0020088510', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('283', '0034989646', 'Rani Waluyo', 'Mamuju, 02 Maret 2003', 'XI IPS 1', 'SMA', '0034989646', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('284', '0031543862', 'Sahrul Gunawan', 'Bone-Bone, 14 Januari 2003', 'XI IPS 1', 'SMA', '0031543862', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('285', '0031914190', 'A.Muhammad Wildan Asyraf', 'Makassar, 21 Agustus 2003', 'XI IPS 2', 'SMA', '0031914190', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('286', '3035183368', 'Alfian Ramadan', 'Sangatta, 23 November 2003', 'XI IPS 2', 'SMA', '3035183368', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('287', '0036148695', 'Andhika Fajrin Ramadhan', 'Pangkajene, 09 November 2003', 'XI IPS 2', 'SMA', '0036148695', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('288', '0032919947', 'Andi Afdal Wahid', 'Mamuju, 31 Maret 2003', 'XI IPS 2', 'SMA', '0032919947', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('289', '0031914152', 'Andi Muhammad Raidh Hanif', 'Bulukumba, 31 Maret 2003', 'XI IPS 2', 'SMA', '0031914152', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('290', '0046699202', 'Andi Sulfahmi', 'Bantaeng, 24 April 2004', 'XI IPS 2', 'SMA', '0046699202', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('291', '0030773167', 'Dewa Rescue Virgiawansyah', 'Sorowako, 28 Juli 2003', 'XI IPS 2', 'SMA', '0030773167', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('292', '0015761947', 'Imam Permana', 'Lambatu, 30 Januari 2001', 'XI IPS 2', 'SMA', '0015761947', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('293', '3043746493', 'Moh. Yusri Idrus', 'Bungku, 05 April 2004', 'XI IPS 2', 'SMA', '3043746493', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('294', '0030432587', 'Muh. Rafly Eka Saputra', 'Jeneponto, 21 Mei 2003', 'XI IPS 2', 'SMA', '0030432587', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('295', '0045029566', 'Muhammad Rifki Tahir', 'Pare-Pare, 28 Februari 2004', 'XI IPS 2', 'SMA', '0045029566', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('296', '0029960697', 'Muhammad. Dzaky Ramadhan Hanafi', 'Makassar, 01 November 2002', 'XI IPS 2', 'SMA', '0029960697', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('297', '0026817801', 'Rahmat Sulaiman', 'Makassar, 27 Desember 2002', 'XI IPS 2', 'SMA', '0026817801', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('298', '0030978251', 'Syahril Habib Muh. Jufri', 'Jayapura, 11 Maret 2003', 'XI IPS 2', 'SMA', '0030978251', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('299', '0028775606', 'Triyadi Ilham Saputra', 'Makassar, 07 Desember 2002', 'XI IPS 2', 'SMA', '0028775606', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('300', '0025815979', 'Wahyu Dwi Putra', 'Camba, 07 Desember 2002', 'XI IPS 2', 'SMA', '0025815979', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('301', '0021838037', 'Muh. Taufiq', 'Salaonro, 25 Juni 2002', 'XI IPS 2', 'SMA', '0021838037', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('302', '3031643852', 'Muhammad Safwan', 'Pare-Pare, 18 Februari 2003', 'XI IPS 2', 'SMA', '3031643852', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('303', '0032879307', 'Alif Yasir', 'Makassar, 10 April 2003', 'XI MIPA', 'SMA', '0032879307', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('304', '0036036014', 'Angga Farhan Sayid Saputra Yusuf', 'Nabire, 13 April 2004', 'XI MIPA', 'SMA', '0036036014', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('305', '0034936325', 'Baso Al Dirham', 'Makassar, 20 Mei 2008', 'XI MIPA', 'SMA', '0034936325', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('306', '0032199157', 'Chairil Aswar', 'Makassar, 10 Juli 2003', 'XI MIPA', 'SMA', '0032199157', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('307', '0037507255', 'Fariz Syamsu Ma\'arif', 'Makassar, 30 Januari 2003', 'XI MIPA', 'SMA', '0037507255', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('308', '0037259126', 'Hikmal Rudiansyah', 'Soppeng, 02 Januari 2003', 'XI MIPA', 'SMA', '0037259126', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('309', '0035283569', 'Indra Wira Yudha', 'Parepare, 02 November 2003', 'XI MIPA', 'SMA', '0035283569', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('310', '0041738375', 'M. Rafly Risqullah Rahman', 'Sidrap, 29 Maret 2004', 'XI MIPA', 'SMA', '0041738375', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('311', '3035675818', 'M. Safri Asri Farhan', 'Masohi, 19 November 2003', 'XI MIPA', 'SMA', '3035675818', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('312', '0032000829', 'M.Ali Akbar', 'Puundoho, 29 November 2003', 'XI MIPA', 'SMA', '0032000829', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('313', '0039914205', 'Muh. Aid Adhani Agil', 'Makassar, 18 Mei 2003', 'XI MIPA', 'SMA', '0039914205', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('314', '0038790762', 'Muh. Ariq Hausan', 'Patoloan, 14 April 2003', 'XI MIPA', 'SMA', '0038790762', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('315', '0044408094', 'Muh. Iksan Ihwan', 'Sorowako, 21 Juli 2004', 'XI MIPA', 'SMA', '0044408094', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('316', '0029859693', 'Muhammad Abrar', 'Pare-Pare, 14 Maret 2002', 'XI MIPA', 'SMA', '0029859693', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('317', '0032561227', 'Muhammad Fadhel Mahram', 'Jakarta, 09 Maret 2004', 'XI MIPA', 'SMA', '0032561227', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('318', '0034952597', 'Muhammad Ikhsan', 'Bonde, 22 Juni 2003', 'XI MIPA', 'SMA', '0034952597', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('319', '0027226143', 'Muhammad Iqbal', 'Sorowako, 10 Desember 2002', 'XI MIPA', 'SMA', '0027226143', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('320', '0030372652', 'Muhammad Nasyrullah Eddy', 'Wamena, 05 Agustus 2003', 'XI MIPA', 'SMA', '0030372652', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('321', '0030432606', 'Muhammad Naufal Ikhwan', 'Makassar, 19 September 2003', 'XI MIPA', 'SMA', '0030432606', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('322', '0029873407', 'Muhammad Nur Fajri', 'Makassar, 11 September 2002', 'XI MIPA', 'SMA', '0029873407', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('323', '0024457968', 'Naufal Abiyyu Supriadi', 'Samarinda, 02 Agustus 2002', 'XI MIPA', 'SMA', '0024457968', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('324', '0038374092', 'Sean Ahmad Firdaus Syihab', 'Bogor, 13 Juli 2003', 'XI MIPA', 'SMA', '0038374092', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('325', '0023879329', 'A. Muhammad Haliq Mubarak', 'Pare-Pare, 13 September 2002', 'XII IPS', 'SMA', '0023879329', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('326', '0023709474', 'Ahmad Fauzy B', 'Pare Pare, 08 Januari 2002', 'XII IPS', 'SMA', '0023709474', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('327', '0030938778', 'Ahmad Kil Ali Nizar', 'Makassar, 30 Maret 2003', 'XII IPS', 'SMA', '0030938778', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('328', '0010568743', 'Ahmad Nu\'ainah Azis', 'Palu, 27 November 2001', 'XII IPS', 'SMA', '0010568743', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('329', '0023477619', 'Andi M. Fayiz Haq', 'Taretta, 08 Januari 2002', 'XII IPS', 'SMA', '0023477619', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('330', '0023067762', 'Andi Muh. Amzul Gairil Amrul', 'Belopa, 02 Desember 2002', 'XII IPS', 'SMA', '0023067762', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('331', '0027408701', 'Andi Muhammad Said', 'Kendari, 05 Oktober 2002', 'XII IPS', 'SMA', '0027408701', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('332', '0020410599', 'Asward Anas', 'Sumbawa, 15 Juli 2002', 'XII IPS', 'SMA', '0020410599', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('333', '0023736576', 'Bima Wahyu Pratama', 'Timika, 02 Juli 2002', 'XII IPS', 'SMA', '0023736576', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('334', '0017711802', 'Lalu Muhammad Imam Jawadi', 'Mangkung, 20 September 2001', 'XII IPS', 'SMA', '0017711802', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('335', '0020234590', 'Muh. Farhan Syach', 'Makassar, 10 Maret 2002', 'XII IPS', 'SMA', '0020234590', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('336', '0020517725', 'Muh. Fikri Alkautsar A', 'Bantaeng, 16 Agustus 2002', 'XII IPS', 'SMA', '0020517725', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('337', '0027702092', 'Muh. Hilal Fakhri Nur', 'Makassar, 07 Februari 2002', 'XII IPS', 'SMA', '0027702092', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('338', '0023491272', 'Muh. Naufal Khalish', 'Makassar, 28 Juli 2002', 'XII IPS', 'SMA', '0023491272', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('339', '0025992418', 'Muh. Nur Ruslan', 'Pinrang, 13 Maret 2002', 'XII IPS', 'SMA', '0025992418', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('340', '0029999883', 'Muhammad Hasyim', 'Pinrang, 27 Mei 2002', 'XII IPS', 'SMA', '0029999883', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('341', '0030139483', 'Muhammad Rum Apriyanto', 'Makale, 22 Februari 2003', 'XII IPS', 'SMA', '0030139483', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('342', '0022668880', 'Nurul Ikhsan ', 'Takalar, 15 Juni 2002', 'XII IPS', 'SMA', '0022668880', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('343', '0022212847', 'Raihan Safwan', 'Maros, 11 Februari 2002', 'XII IPS', 'SMA', '0022212847', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('344', '3026562732', 'Rasyid Aminullah Mustaqim', 'Soroako, 07 Juli 2002', 'XII IPS', 'SMA', '3026562732', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('345', '3029152799', 'Subhan Faris Rumbaroa', 'Masohi, 02 Januari 2002', 'XII IPS', 'SMA', '3029152799', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('346', '0020997180', 'Wahyudi', 'Pinrang, 10 Oktober 2002', 'XII IPS', 'SMA', '0020997180', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('347', '0023106620', 'Zaki Maulana Situru', 'Masohi, 24 Juli 2002', 'XII IPS', 'SMA', '0023106620', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('348', '0020677831', 'Aan Adriyansyir Putra', 'Labuan Bajo, 07 April 2002', 'XII MIPA', 'SMA', '0020677831', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('349', '0023438588', 'Adjie Surya Kartanegara', 'Bulukumba, 05 Juli 2002', 'XII MIPA', 'SMA', '0023438588', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('350', '3024802044', 'Adriyan Tilawah Saputra Batara', 'Mataram, 09 Juli 2002', 'XII MIPA', 'SMA', '3024802044', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('351', '0021569774', 'Alif Sulthan Syahputra', 'Makassar, 19 Agustus 2002', 'XII MIPA', 'SMA', '0021569774', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('352', '0018282102', 'Andi Abimayu Arief Paddengeng', 'Manokwari, 22 Juli 2001', 'XII MIPA', 'SMA', '0018282102', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('353', '0025410025', 'Anugrah Pratama S', 'Bantaeng, 14 Mei 2002', 'XII MIPA', 'SMA', '0025410025', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('354', '0021934661', 'M. Fadil Alamsyah', 'Makassar, 20 Juli 2002', 'XII MIPA', 'SMA', '0021934661', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('355', '0016936212', 'Muh. Arief Furqan Abdullah', 'Pinrang, 05 Desember 2001', 'XII MIPA', 'SMA', '0016936212', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('356', '0020454319', 'Muh. Fiqri Maulana Ibrahim', 'Makassar, 26 Mei 2002', 'XII MIPA', 'SMA', '0020454319', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('357', '0025874079', 'Muh. Rifqi Akbar', 'Paayumang, 21 Agustus 2002', 'XII MIPA', 'SMA', '0025874079', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('358', '0026284283', 'Muhammad Ahsan As\'ad', 'Surabaya, 08 April 2002', 'XII MIPA', 'SMA', '0026284283', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('359', '0028472741', 'Muhammad Alif H', 'Sorowako, 27 Mei 2002', 'XII MIPA', 'SMA', '0028472741', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('360', '0021979725', 'Nur Alim Amir', 'Mamuju, 08 Desember 2002', 'XII MIPA', 'SMA', '0021979725', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('361', '0024006522', 'Oki Mahesa', 'Makassar, 05 Oktober 2002', 'XII MIPA', 'SMA', '0024006522', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('362', '0016235584', 'Taqbir Hasan Ramadani', 'Malimpung, 15 Desember 2001', 'XII MIPA', 'SMA', '0016235584', '2019-12-20 00:22:53', '2019-12-20 00:22:55');
INSERT INTO `pl_santri` VALUES ('363', '0021984974', 'Wahyu', 'Awo, 15 Januari 2002', 'XII MIPA', 'SMA', '0021984974', '2019-12-20 00:22:53', '2019-12-20 00:22:55');

-- ----------------------------
-- Table structure for pl_status
-- ----------------------------
DROP TABLE IF EXISTS `pl_status`;
CREATE TABLE `pl_status` (
  `idstatus` int(10) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pl_status
-- ----------------------------
INSERT INTO `pl_status` VALUES ('1', 'Dipinjam', '2019-12-20 00:34:00', '2019-12-20 00:34:02');
INSERT INTO `pl_status` VALUES ('2', 'Dikembalikan', '2019-12-20 00:34:11', '2019-12-20 00:34:14');
