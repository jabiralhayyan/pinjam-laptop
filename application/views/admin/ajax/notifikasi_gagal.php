
<div class="alert alert-primary background-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
    </button>
    Maaf ! NISN Santri tidak ditemukan, Mohon periksa kembali
</div>