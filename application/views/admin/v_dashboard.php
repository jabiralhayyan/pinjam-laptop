<?php
        require_once('layout/head.php');
        require_once('layout/navbar.php');
        require_once('layout/sidebar.php');
?>



                    
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">

                                            <!-- statustic-card start -->
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-yellow text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Jumlah Santri</p>
                                                                <h4 class="m-b-0"><?=$jumlahsantri;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-user f-50 text-c-yellow"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-green text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Jumlah MA</p>
                                                                <h4 class="m-b-0"><?=$jumlahsantrima;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-credit-card f-50 text-c-green"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-pink text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Jumlah SMA</p>
                                                                <h4 class="m-b-0"><?=$jumlahsantrisma;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-book f-50 text-c-pink"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Belum Dikembalikan</p>
                                                                <h4 class="m-b-0"><?=$jumlahpinjam;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-shopping-cart f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- statustic-card start -->

                                            <!-- statustic-card start -->
                                            <div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <div class="card-header-left ">
                                                            <h4>Statistik Peminjaman Tahun <?=date('Y');?></h4>
                                                        </div>
                                                    </div>
                                                    <div class="card-block-big">
                                                        <div id="morris-bar-chart"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- statustic-card start -->

                                        </div>
                                    </div>
                                </div>

     
     <script>
         "use strict";
// Morris bar chart
Morris.Bar({
    element: 'morris-bar-chart',
    data: [{
        y: 'Januari',
        a: <?=$pinjam_bulan1;?>
    }, {
        y: 'Februari',
        a: <?=$pinjam_bulan2;?>
    }, {
        y: 'Maret',
        a: <?=$pinjam_bulan3;?>
    }, {
        y: 'April',
        a: <?=$pinjam_bulan3;?>
    }, {
        y: 'Mei',
        a: <?=$pinjam_bulan4;?>
    }, {
        y: 'Juni',
        a: <?=$pinjam_bulan5;?>
    }, {
        y: 'Juli',
        a: <?=$pinjam_bulan6;?>
    }, {
        y: 'Agustus',
        a: <?=$pinjam_bulan7;?>
    }, {
        y: 'September',
        a: <?=$pinjam_bulan8;?>
    }, {
        y: 'Oktober',
        a: <?=$pinjam_bulan9;?>
    }, {
        y: 'November',
        a: <?=$pinjam_bulan10;?>
    }, {
        y: 'Desember',
        a: <?=$pinjam_bulan12;?>
    }
    ],
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Pinjam'],
    barColors: ['#5FBEAA'],
    hideHover: 'auto',
    gridLineColor: '#eef0f2',
    resize: true
});


     </script>                           

    <?php
        require_once('layout/script.php');
    ?>


