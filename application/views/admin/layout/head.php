<!DOCTYPE html>
<html lang="en">

<head>
    <title><?=$title;?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=base_url();?>files\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\bower_components\bootstrap\css\bootstrap.min.css">
    <!-- UPLOADS -->
    <link href="<?=base_url();?>files\assets\pages\jquery.filer\css\jquery.filer.css" type="text/css" rel="stylesheet" />
    <link href="<?=base_url();?>files\assets\pages\jquery.filer\css\themes\jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\icon\themify-icons\themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\icon\icofont\css\icofont.css">
    <!-- radial chart.css -->
    <link rel="stylesheet" href="<?=base_url();?>files\assets\pages\chart\radial\css\radial.css" type="text/css" media="all">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\icon\feather\css\feather.css">
    <!-- Chartlist chart css -->
    <link rel="stylesheet" href="<?=base_url();?>files\bower_components\chartist\css\chartist.css" type="text/css" media="all">
    <!-- Datatable CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\pages\data-table\css\buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\css\jquery.mCustomScrollbar.css">

    <!-- Required Jquery -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery\js\jquery.min.js"></script>
    <!-- Chartlist charts -->
    <script src="<?=base_url();?>files\bower_components\chartist\js\chartist.js"></script>
    <script src="<?=base_url();?>files\assets\pages\chart\chartlist\js\chartist-plugin-threshold.js"></script>
    <!-- Morris Chart js -->
    <script src="<?=base_url();?>files\bower_components\raphael\js\raphael.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\morris.js\js\morris.js"></script>

</head>
<!-- Menu sidebar static layout -->
<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->

        <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">