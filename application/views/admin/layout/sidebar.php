<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Menu</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="<?=$dashboard_active;?>">
                                    <a href="<?=base_url();?>dashboard">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Dashboard</span>
                                    </a>
                                </li>
                                <li class="<?=$pengembalian_active;?>">
                                    <a href="<?=base_url();?>pengembalian">
                                        <span class="pcoded-micon"><i class="icofont icofont-refresh"></i></span>
                                        <span class="pcoded-mtext">Pengembalian</span>
                                    </a>
                                </li>
                                <li class="<?=$peminjaman_active;?>">
                                    <a href="<?=base_url();?>peminjaman">
                                        <span class="pcoded-micon"><i class="icofont icofont-book-alt"></i></span>
                                        <span class="pcoded-mtext">Peminjaman</span>
                                    </a>
                                </li>
                                <li class="<?=$datasantri_active;?>">
                                    <a href="<?=base_url();?>datasantri">
                                        <span class="pcoded-micon"><i class="icofont icofont-papers"></i></span>
                                        <span class="pcoded-mtext">Data Santri</span>
                                    </a>
                                </li>
                                <li class="<?=$datasantri_active;?>">
                                    <a href="<?=base_url();?>laporan">
                                        <span class="pcoded-micon"><i class="icofont icofont-book"></i></span>
                                        <span class="pcoded-mtext">Laporan Peminjaman</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel">Setting</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="<?=$konfigurasi_active;?>">
                                    <a href="<?=base_url();?>konfigurasi">
                                        <span class="pcoded-micon"><i class="icofont icofont-settings"></i></span>
                                        <span class="pcoded-mtext">Konfigurasi</span>
                                    </a>
                                </li>
                                <li class="<?=$profil_active;?>">
                                    <a href="<?=base_url();?>admin/profil">
                                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                        <span class="pcoded-mtext">Profil</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?=base_url();?>admin/logout">
                                        <span class="pcoded-micon"><i class="icofont icofont-ui-lock"></i></span>
                                        <span class="pcoded-mtext">Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>

<div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">