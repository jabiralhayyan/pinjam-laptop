   <!-- <div id="styleSelector"> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Ends -->
    
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\bootstrap\js\bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\modernizr\js\css-scrollbars.js"></script>
    <!-- UPLOADS -->
    <script src="<?=base_url();?>files\assets\pages\jquery.filer\js\jquery.filer.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>files\assets\pages\filer\custom-filer.js" type="text/javascript"></script>
    <script src="<?=base_url();?>files\assets\pages\filer\jquery.fileuploads.init.js" type="text/javascript"></script>
    <!-- Datatable -->
    <script src="<?=base_url();?>files\bower_components\datatables.net\js\jquery.dataTables.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js"></script>
    <script src="<?=base_url();?>files\assets\pages\data-table\js\jszip.min.js"></script>
    <script src="<?=base_url();?>files\assets\pages\data-table\js\pdfmake.min.js"></script>
    <script src="<?=base_url();?>files\assets\pages\data-table\js\vfs_fonts.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-buttons\js\buttons.print.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-buttons\js\buttons.html5.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js"></script>
    <script src="<?=base_url();?>files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\chart.js\js\Chart.js"></script>
    <!-- Google map js -->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="<?=base_url();?>files\assets\pages\google-maps\gmaps.js"></script>
    <!-- gauge js -->
    <script src="<?=base_url();?>files\assets\pages\widget\gauge\gauge.min.js"></script>
    <script src="<?=base_url();?>files\assets\pages\widget\amchart\amcharts.js"></script>
    <script src="<?=base_url();?>files\assets\pages\widget\amchart\serial.js"></script>
    <script src="<?=base_url();?>files\assets\pages\widget\amchart\gauge.js"></script>
    <script src="<?=base_url();?>files\assets\pages\widget\amchart\pie.js"></script>
    <script src="<?=base_url();?>files\assets\pages\widget\amchart\light.js"></script>
    <script src="<?=base_url();?>files\assets\pages\data-table\js\data-table-custom.js" type="text/javascript"></script>
    <!-- Custom js -->
    <!-- <script src="<?=base_url();?>files\assets\pages\chart\morris\morris-custom-chart.js"></script> -->
    <script src="<?=base_url();?>files\assets\js\pcoded.min.js"></script>
    <script src="<?=base_url();?>files\assets\js\vartical-layout.min.js"></script>
    <script src="<?=base_url();?>files\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\assets\pages\dashboard\crm-dashboard.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\assets\js\script.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>

</html>