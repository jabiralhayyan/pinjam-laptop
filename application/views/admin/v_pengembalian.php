<?php
require_once('layout/head.php');
require_once('layout/navbar.php');
require_once('layout/sidebar.php');

function tanggal_format($tanggal) {
    if($tanggal=='' || $tanggal==NULL) return NULL;
    else {
        $split = explode('-', $tanggal);
        $tanggal = $split[2];
        $bulan = $split[1];
        $tahun = $split[0];
        $tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
        return $tanggal_indo;
    }
}

?>

<div class="page-wrapper">
    <div class="page-body">
        <div class="row">
            <!-- statustic-card start -->
            <div class="col-xl-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header">
                            <!-- Notifikasi -->
                            <?php
                            if($notifikasi_berhasil) {
                                ?>
                                <div class="alert alert-primary background-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <?=$notifikasi_berhasil;?>
                                </div>
                            <?php } ?>
                            <?php
                            if($notifikasi_gagal) {
                                ?>
                                <div class="alert alert-primary background-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <?=$notifikasi_gagal;?>
                                </div>
                            <?php } ?>

                            <div class="card-header-left ">
                                <h4>Peminjaman Laptop</h4>
                            </div>
                            
                        <div class="card-header-left ">
                            <h4>Pengembalian Laptop</h4>
                        </div>
                    </div>
                    <div class="card-block-big">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>Nama Peminjam</th>
                                        <th>Kelas</th>
                                        <th>Lembaga</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        if($query->num_rows() > 0 ) {
                                            foreach ($query->result() as $row) {
                                                $tanggal = substr($row->tanggalpinjam, 0, 10);
                                                $waktu = substr($row->tanggalpinjam, 11, 18);
                                                $tanggalpinjam = tanggal_format($tanggal);
                                    ?>
                                    <tr>
                                        <td><?=$no;?></td>
                                        <td><?=$row->nisn;?></td>
                                        <td><?=$row->nama;?></td>
                                        <td><?=$row->kelas;?></td>
                                        <td><?=$row->lembaga;?></td>
                                        <td><?=$tanggalpinjam;?> | <?=$waktu;?></td>
                                        <td>
                                            <?php if($row->idstatus == 1){ ?>
                                            <div class="label-main">
                                                <label class="label label-lg label-warning">Dipinjam</label>
                                            </div>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary btn-round waves-effect" data-toggle="modal" data-target="#pengembalian<?=$row->iddatapinjam;?>"><i class="icofont icofont-hand-up"></i>Pengembalian</button>

                                            <div class="modal fade" id="pengembalian<?=$row->iddatapinjam;?>" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="background-color:#01A9AC">
                                                            <h4 class="modal-title" style="color:white">Pengembalian Laptop</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h5>Apakah anda yakin untuk mengembalikan Laptop ?</h5><br>
                                                            <p><b>
                                                                - Santri Bernama : <?=$row->nama;?><br>
                                                                - Kelas : <?=$row->kelas;?><br>
                                                                - Lembaga : <?=$row->lembaga;?><br>
                                                            </b>
                                                            </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                            <form action="<?=base_url();?>pengembalian/dopengembalian" method="post">
                                                                <input type="hidden" name="iddatapinjam" value="<?=$row->iddatapinjam;?>">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light ">Simpan</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $no++; }} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>Nama Peminjam</th>
                                        <th>Kelas</th>
                                        <th>Lembaga</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- statustic-card start -->



    </div>
</div>
</div>



<?php
require_once('layout/script.php');
?>


