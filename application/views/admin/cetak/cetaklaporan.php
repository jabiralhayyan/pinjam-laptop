<?php 
 //membuat objek PHPExcel
$objPHPExcel = new PHPExcel();

            //set Sheet yang akan diolah 
/*
 //First sheet
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets
    
    $countsheet=0;
    while ($countsheet < 3) {

    // Add new sheet
    $objWorkSheet = $objPHPExcel->createSheet($countsheet); //Setting index when creating
    

    // Rename sheet
    $objWorkSheet->setTitle("$countsheet");

    $countsheet++;
    }
*/

    $objPHPExcel->setActiveSheetIndex(0);
    //header
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0 , 1, 'No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1 , 1, 'NISN');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2 , 1, 'Nama Santri');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3 , 1, 'Kelas ');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4 , 1, 'Lembaga');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5 , 1, 'Tanggal Pinjam');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6 , 1, 'Tanggal Kembali');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7 , 1, 'Status');
    
    $i=2;
    foreach ($query->result() as $row){
        if($row->idstatus == '1') $status='Dipinjam';
        if($row->idstatus == '2') $status='Dikembalikan';
    //isi atau content
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0 ,  $i, $i-1);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1 ,  $i, $row->nisn);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2 ,  $i, $row->nama);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3 ,  $i, $row->kelas);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4 ,  $i, $row->lembaga);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5 ,  $i, $row->tanggalpinjam);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6 ,  $i, $row->tanggalkembali);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7 ,  $i, $status);
    $i++;
    }

    $objPHPExcel->setActiveSheetIndex(0);

            //change the font size
    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setSize(12);
			//make the font become bold
    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
            //set Color
    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('92D14F');

            //set title pada sheet (me rename nama sheet)
    $objPHPExcel->getActiveSheet()->setTitle($kelas.' ('.$lembaga.')'); //dijadikan nama kelompok

            //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            //sesuaikan headernya 
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //ubah nama file saat diunduh
    header('Content-Disposition: attachment;filename="Lapoan Peminjaman Kelas-'.$kelas.' ('.$lembaga.').xlsx"');
            //unduh file
    $objWriter->save("php://output");

            //Mulai dari create object PHPExcel itu ada dokumentasi lengkapnya di PHPExcel, 
            // Folder Documentation dan Example
            // untuk belajar lebih jauh mengenai PHPExcel silakan buka disitu
    ?>