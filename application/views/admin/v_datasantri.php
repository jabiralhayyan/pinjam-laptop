<?php
require_once('layout/head.php');
require_once('layout/navbar.php');
require_once('layout/sidebar.php');
?>



<div class="page-wrapper">
    <div class="page-body">
        <div class="row">


            <!-- statustic-card start -->
            <div class="col-xl-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <!-- Notifikasi -->
                        <?php
                        if($notifikasi_berhasil) {
                        ?>
                        <div class="alert alert-primary background-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled text-white"></i>
                            </button>
                            <?=$notifikasi_berhasil;?>
                        </div>
                        <?php } ?>
                        <?php
                        if($notifikasi_gagal) {
                        ?>
                        <div class="alert alert-primary background-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled text-white"></i>
                            </button>
                            <?=$notifikasi_gagal;?>
                        </div>
                        <?php } ?>

                        <div class="card-header-left ">
                            <h4>Data Santri</h4>
                        </div>
                        <br><br>
                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#tambah"><i class="icofont icofont-plus-circle"></i>Tambah Santri</button>

                    </div>
                    <div class="card-block-big">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>Nama</th>
                                        <th>TTL</th>
                                        <th>Kelas</th>
                                        <th>Lembaga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        if($query->num_rows() > 0 ) {
                                            foreach($query->result() as $row) {
                                    ?>
                                    <tr>
                                        <td><?=$no;?></td>
                                        <td><?=$row->nisn;?></td>
                                        <td><?=$row->nama;?></td>
                                        <td><?=$row->ttl;?></td>
                                        <td><?=$row->kelas;?></td>
                                        <td><?=$row->lembaga;?></td>
                                        <td>

                                            <button type="button" class="btn btn-success btn-round waves-effect" data-toggle="modal" data-target="#foto<?=$row->idsantri;?>"><i class="icofont icofont-business-man-alt-2"></i>Foto</button>
                                            <button type="button" class="btn btn-primary btn-round waves-effect" data-toggle="modal" data-target="#edit<?=$row->idsantri;?>"><i class="icofont icofont-edit"></i>Edit</button>
                                            <button type="button" class="btn btn-danger btn-round waves-effect" data-toggle="modal" data-target="#delete<?=$row->idsantri;?>"><i class="icofont icofont-trash"></i>Delete</button>


                                             <!-- FOTO -->
                                            <div class="modal fade" id="foto<?=$row->idsantri;?>" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form action="<?=base_url();?>datasantri/douploadfoto" method="POST" enctype="multipart/form-data">
                                                        <div class="modal-header" style="background-color:#0AC282">
                                                            <h4 class="modal-title" style="color:white">Foto Santri</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <center><h5><b><?=$row->nama;?></b></h5><br>
                                                            <?php 
                                                            if($row->foto != ''){ ?>
                                                                <img src="<?=base_url();?>files/assets/images/santri/<?=$row->foto;?>.JPG" width="200px" height="300px">
                                                            <?php }else{ ?>
                                                                <img src="<?=base_url();?>files/assets/images/santri/user.JPG">
                                                           <?php } ?>
                                                            </center>
                                                            <br>
                                                            <input type="file" name="fileFoto">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                <input type="hidden" name="idsantri" value="<?=$row->idsantri;?>">
                                                                <input type="hidden" name="nisn" value="<?=$row->nisn;?>">
                                                                <button type="submit" class="btn btn-success waves-effect waves-light">Upload Foto</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- EDIT -->
                                            <div class="modal fade" id="edit<?=$row->idsantri;?>" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="background-color:#01A9AC">
                                                            <h4 class="modal-title" style="color:white">Edit Santri</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form id="main" method="post" action="<?=base_url();?>datasantri/doeditdatasantri" novalidate="">
                                                            <input type="hidden" name="idsantri" value="<?=$row->idsantri;?>">
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">NISN <span style="color:red">*</span></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="nisn" placeholder="" value="<?=$row->nisn;?>" required readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Nama</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="nama" value="<?=$row->nama;?>" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">TTL</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="ttl" value="<?=$row->ttl;?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Lembaga</label>
                                                                <div class="col-sm-10">
                                                                    <select name="lembaga" id="lembaga" class="form-control">
                                                                        <?php if($row->lembaga == "MA"){ ?> 
                                                                            <option selected value="MA">MA</option>
                                                                            <option value="SMA">SMA</option>
                                                                        <?php } else{ ?>
                                                                            <option value="MA">MA</option>
                                                                            <option selected value="SMA">SMA</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Kelas</label>
                                                                <div class="col-sm-10">
                                                                    <select name="kelas" class="form-control">
                                                                        <?php 
                                                                        foreach ($kelas->result() as $pl_kelas) {
                                                                        if($pl_kelas->kelas == $row->kelas){ ?>
                                                                            <option selected value="<?=$pl_kelas->kelas;?>"><?=$pl_kelas->kelas;?></option>
                                                                        <?php } else{ ?>
                                                                            <option value="<?=$pl_kelas->kelas;?>"><?=$pl_kelas->kelas;?></option>
                                                                        <?php }} ?>
                                                                    </select>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- DELETE -->
                                            <div class="modal fade" id="delete<?=$row->idsantri;?>" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="background-color:#EE5041">
                                                            <h4 class="modal-title" style="color:white">Delete Santri</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h5>Apakah anda yakin menghapus data santri<br>bernama
                                                            <b>"<?=$row->nama;?>"</b> ?</h5>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                            <form action="<?=base_url();?>datasantri/deletedatasantri" method="POST">
                                                                <input type="hidden" name="idsantri" value="<?=$row->idsantri;?>">
                                                                <button type="submit" class="btn btn-danger waves-effect waves-light">Delete</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>

                                    <?php $no++; }} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>Nama</th>
                                        <th>TTL</th>
                                        <th>Kelas</th>
                                        <th>Lembaga</th>
                                        <th>Action</th>
                                    </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- statustic-card start -->



    </div>
</div>
</div>


<!-- TAMBAH -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#01A9AC">
                <h4 class="modal-title" style="color:white">Tambah Santri</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="main" method="post" action="<?=base_url();?>datasantri/dotambahdatasantri" novalidate="">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NISN <span style="color:red">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nisn" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">TTL</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="ttl" placeholder="Makassar, 20 Juni 1995">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Lembaga</label>
                        <div class="col-sm-10">
                            <select name="lembaga" id="lembaga" class="form-control">
                                    <option value="MA">MA</option>
                                    <option value="SMA">SMA</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kelas</label>
                        <div class="col-sm-10">
                            <select name="kelas" class="form-control">
                                <?php 
                                foreach ($kelas->result() as $pl_kelas) { ?>
                                    <option value="<?=$pl_kelas->kelas;?>"><?=$pl_kelas->kelas;?></option>
                                <?php } ?>
                                </select>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php
require_once('layout/script.php');
?>


