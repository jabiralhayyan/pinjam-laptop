<?php
require_once('layout/head.php');
require_once('layout/navbar.php');
require_once('layout/sidebar.php');

function tanggal_format($tanggal) {
    if($tanggal=='' || $tanggal==NULL) return NULL;
    else {
        $split = explode('-', $tanggal);
        $tanggal = $split[2];
        $bulan = $split[1];
        $tahun = $split[0];
        $tanggal_indo = $tanggal.'-'.$bulan.'-'.$tahun;
        return $tanggal_indo;
    }
}
?>

<div class="page-wrapper">
    <div class="page-body">
        <div class="row">
            <!-- statustic-card start -->
            <div class="col-xl-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header">
                            <!-- Notifikasi -->
                            <?php
                            if($notifikasi_berhasil) {
                                ?>
                                <div class="alert alert-primary background-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <?=$notifikasi_berhasil;?>
                                </div>
                            <?php } ?>
                            <?php
                            if($notifikasi_gagal) {
                                ?>
                                <div class="alert alert-primary background-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                    </button>
                                    <?=$notifikasi_gagal;?>
                                </div>
                            <?php } ?>
                            
                        <div class="card-header-left ">
                            <h4>Laporan Peminjaman</h4>
                        </div>
                    </div>
                    <div class="card-block-big">
                        <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <center><h5><b>CARI BERDASAR LEMBAGA DAN KELAS</b></h5></center>
                                <br>
                                    <form action="<?=base_url();?>laporan" method="POST">
                                        <select name="lembaga" id="lembaga" class="form-control">
                                                <option value="MA">MA</option>
                                                <option value="SMA">SMA</option>
                                        </select>
                                        <select name="kelas" id="kelas" class="form-control">
                                                <?php 
                                                foreach ($kelas->result() as $pl_kelas) { ?>
                                                    <option value="<?=$pl_kelas->kelas;?>"><?=$pl_kelas->kelas;?></option>
                                                <?php } ?>
                                        </select>
                                        <br>
                                        <button type="submit" class="btn btn-primary btn-block" id="basic-addon10">
                                            <span class="" style="color:white"><i class="icofont icofont-search"></i>Cari</span>
                                        </button>
                                    </form>
                                </div>
                        </div>
                        <br>

                        <?php
                            if($query != '') { ?>
                                <h4>Laporan Peminjaman Kelas <?=$_kelas;?> (<?=$_lembaga;?>)</h4>
                                <form method="POST" action="<?=base_url();?>laporan/exportLaporan">
                                    <input type="hidden" name="lembaga" value="<?=$_lembaga;?>">
                                    <input type="hidden" name="kelas" value="<?=$_kelas;?>">
                                    <button type="submit" class="btn btn-success waves-effect"><i class="icofont icofont-download-alt"></i>Download Laporan</button>
                                </form>
                        <?php } ?>
                        <br><br>

                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NISN</th>
                                        <th>Nama Peminjam</th>
                                        <th>Kelas</th>
                                        <th>Lembaga</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        if($query != '') {
                                            foreach ($query->result() as $row) {
                                                $tanggal = substr($row->tanggalpinjam, 0, 10);
                                                $waktu = substr($row->tanggalpinjam, 11, 18);
                                                $tanggalpinjam = tanggal_format($tanggal);

                                                $_tanggal = substr($row->tanggalkembali, 0, 10);
                                                $_waktu = substr($row->tanggalkembali, 11, 18);
                                                $tanggalkembali = tanggal_format($_tanggal);
                                    ?>
                                    <tr>
                                        <td><?=$no;?></td>
                                        <td><?=$row->nisn;?></td>
                                        <td><?=$row->nama;?></td>
                                        <td><?=$row->kelas;?></td>
                                        <td><?=$row->lembaga;?></td>
                                        <td><?=$tanggalpinjam;?> | <?=$waktu;?></td>
                                        <td>
                                            <?php if($tanggalkembali !=''){ ;?>
                                            <?=$tanggalkembali;?> | <?=$_waktu;?>
                                            <?php } 
                                            else {
                                                echo '<center>-</center>';
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php if($row->idstatus == 1){ ?>
                                            <div class="label-main">
                                                <label class="label label-lg label-warning">Dipinjam</label>
                                            </div>
                                            <?php } ?>
                                            <?php if($row->idstatus == 2){ ?>
                                            <div class="label-main">
                                                <label class="label label-lg label-success">Dikembalikan</label>
                                            </div>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $no++; }} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                   <th>No</th>
                                   <th>NISN</th>
                                   <th>Nama Peminjam</th>
                                   <th>Kelas</th>
                                   <th>Lembaga</th>
                                   <th>Tanggal Pinjam</th>
                                   <th>Tanggal Kembali</th>
                                   <th>Status</th> 
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- statustic-card start -->



    </div>
</div>
</div>



<?php
require_once('layout/script.php');
?>


