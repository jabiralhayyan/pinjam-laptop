<?php
        require_once('layout/head.php');
        require_once('layout/navbar.php');
        require_once('layout/sidebar.php');
?>



                    
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">

                                            <!-- statustic-card start -->
                                            <div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <!-- Notifikasi -->
                                                        <?php
                                                        if($notifikasi_berhasil) {
                                                        ?>
                                                        <div class="alert alert-primary background-success">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_berhasil;?>
                                                        </div>
                                                        <?php } ?>
                                                        <?php
                                                        if($notifikasi_gagal) {
                                                        ?>
                                                        <div class="alert alert-primary background-danger">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_gagal;?>
                                                        </div>
                                                        <?php } ?>

                                                        <div class="card-header-left ">
                                                            <h4>Peminjaman Laptop</h4>
                                                        </div>
                                                    </div>
                                                    <div class="card-block-big">
                                                        <div class="form-group row">
                                                            <div class="col-sm-3"></div>
                                                                <h5>Cari Berdasar NISN</h5>
                                                                <div class="col-sm-4">
                                                                    <form action="<?=base_url();?>peminjaman" method="POST">
                                                                    <div class="input-group input-group-button">
                                                                        <input type="text" class="form-control" name="nisn" required>
                                                                        <button type="submit" class="input-group-addon btn btn-primary" id="basic-addon10">
                                                                            <span class=""><i class="icofont icofont-search"></i>Cari</span>
                                                                        </button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                        </div>
                                                        <br>

                                                        <?php if($query != ''){ ?>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">NISN</label>
                                                                <div class="col-sm-4">
                                                                        <input type="text" class="form-control" name="nisn" value="<?=$nisn;?>" placeholder="" readonly>
                                                                </div>
                                                            </div>                                                   
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Nama</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="nama" value="<?=$nama;?>" placeholder="" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Lembaga</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="lembaga" value="<?=$lembaga;?>" placeholder="" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Kelas</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="kelas" value="<?=$kelas;?>" placeholder="" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">TTL</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="ttl" value="<?=$ttl;?>" placeholder="" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                <center>
                                                                    <button type="button" class="btn btn-primary btn-lg waves-effect" data-toggle="modal" data-target="#pinjam"><i class="icofont icofont-plus-circle"></i>Pinjam</button>
                                                                </center>
                                                                </div>
                                                            </div>

                                                                <!-- MODAL PINJAM -->
                                                                <div class="modal fade" id="pinjam" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header" style="background-color:#01A9AC">
                                                                                <h4 class="modal-title" style="color:white">Pinjam Laptop</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h5>Apakah anda yakin meminjamkan laptop kepada santri<br>bernama <b>"<?=$nama;?>"</b>?</h5>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                <form action="<?=base_url();?>peminjaman/dopinjam" method="POST">
                                                                                    <input type="hidden" name="idsantri" value="<?=$idsantri;?>">
                                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Ya</a>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  

                                                        <?php } ?>

                                                         </div>
                                                </div>
                                            </div>
                                            <!-- statustic-card start -->
                                        </div>
                                    </div>
                                </div>

                  

    <?php
        require_once('layout/script.php');
    ?>


