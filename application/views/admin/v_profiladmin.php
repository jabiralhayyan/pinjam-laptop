<?php
        require_once('layout/head.php');
        require_once('layout/navbar.php');
        require_once('layout/sidebar.php');
?>



                    
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">

                                            <!-- statustic-card start -->
                                            <div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <!-- Notifikasi -->
                                                        <?php
                                                        if($notifikasi_berhasil) {
                                                        ?>
                                                        <div class="alert alert-primary background-success">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_berhasil;?>
                                                        </div>
                                                        <?php } ?>
                                                        <?php
                                                        if($notifikasi_gagal) {
                                                        ?>
                                                        <div class="alert alert-primary background-danger">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_gagal;?>
                                                        </div>
                                                        <?php } ?>



                                                        <div class="card-header-left ">
                                                            <h4>Profil Admin</h4>
                                                        </div>
                                                    </div>
                                                    <div class="card-block-big">
                                                        <form method="POST" action="<?=base_url();?>admin/doupdateprofil">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Username</label>
                                                                <div class="col-sm-4">
                                                                        <input type="text" class="form-control" name="username" value="<?=$username;?>" placeholder="" required readonly>
                                                                </div>
                                                            </div>                                                   
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Nama</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="nama" value="<?=$nama;?>" placeholder="" required> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Password</label>
                                                                <div class="col-sm-4">
                                                                    <input type="password" class="form-control" name="password" value="<?=$password;?>" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Konfirmasi Password</label>
                                                                <div class="col-sm-4">
                                                                    <input type="password" class="form-control" name="konfirmasi" value="" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                <center>
                                                                    <button type="submit" class="btn btn-primary btn-lg waves-effect"><i class="icofont icofont-plus-circle"></i>Simpan</button>
                                                                </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                         </div>
                                                </div>
                                            </div>
                                            <!-- statustic-card start -->
                                        </div>
                                    </div>
                                </div>


    <?php
        require_once('layout/script.php');
    ?>


