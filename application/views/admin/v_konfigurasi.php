<?php
        require_once('layout/head.php');
        require_once('layout/navbar.php');
        require_once('layout/sidebar.php');
?>



                    
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">

                                            <!-- statustic-card start -->
                                            <div class="col-xl-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-header">

                                                        <!-- Notifikasi -->
                                                        <?php
                                                        if($notifikasi_berhasil) {
                                                        ?>
                                                        <div class="alert alert-primary background-success">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_berhasil;?>
                                                        </div>
                                                        <?php } ?>
                                                        <?php
                                                        if($notifikasi_gagal) {
                                                        ?>
                                                        <div class="alert alert-primary background-danger">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled text-white"></i>
                                                            </button>
                                                            <?=$notifikasi_gagal;?>
                                                        </div>
                                                        <?php } ?>

                                                        <div class="card-header-left ">
                                                            <h4>Konfigurasi Sistem</h4>
                                                        </div>
                                                    </div>
                                                    <div class="card-block-big">
                                                        <form method="POST" action="<?=base_url();?>konfigurasi/doeditkonfigurasi">
                                                        <input type="hidden" name="idkonfigurasi" value="<?=$idkonfigurasi;?>">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Nama Sistem<span style="color:red">*</span></label>
                                                                <div class="col-sm-4">
                                                                        <input type="text" class="form-control" name="namasistem" value="<?=$namasistem;?>" placeholder="" required>
                                                                </div>
                                                            </div>                                                   
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Instansi</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="instansi" value="<?=$instansi;?>" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Alamat</label>
                                                                <div class="col-sm-4">
                                                                    <textarea type="text" class="form-control" name="alamat" placeholder=""><?=$alamat;?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">Email</label>
                                                                <div class="col-sm-4">
                                                                    <input type="email" class="form-control" name="email" value="<?=$email;?>" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-3"></div>
                                                                <label class="col-sm-1 col-form-label">No Telepon</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="notelepon" value="<?=$notelepon;?>" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                <center>
                                                                    <button type="submit" class="btn btn-primary btn-lg waves-effect"><i class="icofont icofont-plus-circle"></i>Simpan</button>
                                                                </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                         </div>
                                                </div>
                                            </div>
                                            <!-- statustic-card start -->
                                        </div>
                                    </div>
                                </div>


    <?php
        require_once('layout/script.php');
    ?>


