
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?=$title;?></title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=base_url();?>files\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\bower_components\bootstrap\css\bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\icon\themify-icons\themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\icon\icofont\css\icofont.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>files\assets\css\style.css">
</head>

<body class="fix-menu">

    <!-- Pre-loader start -->
    <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
    


    <!-- Pre-loader end -->
    <section class="login-block">

        <!-- Container-fluid starts -->

        <div class="container-fluid">
             

                        
            <div class="row" style="margin-top: -200px">

                
                <div class="col-sm-3"></div>
                <div class="col-sm-6"><!-- 
                        <div class="text-center">
                                <img src="<?=base_url();?>files\assets\images\immim-logo.png" width="" height="">
                            </div> -->
                    <!-- Authentication card start -->
    
                        <div id="notifikasi"></div>

                        <?php if($notifikasi_gagal) { ?>
                            <div class="alert alert-primary background-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <i class="icofont icofont-close-line-circled text-white"></i>
                                </button>
                                <?=$notifikasi_gagal;?>
                            </div>
                        <?php }if($notifikasi_pinjam){ ?>
                            <div class="alert alert-primary background-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled text-white"></i>
                                </button>
                                <div class="table-responsive">
                                    <table class="table m-0" style="border:0">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <img src="<?=base_url();?>files/assets/images/santri/<?=$foto;?>.JPG" width="70px" hight="105px">
                                                </th>
                                                <td style="font-size: 18px"><?=$notifikasi_pinjam;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } if($notifikasi_pengembalian) { ?>
                            <div class="alert alert-primary background-primary">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled text-white"></i>
                                </button>
                                <div class="table-responsive">
                                    <table class="table m-0" style="border:0">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <img src="<?=base_url();?>files/assets/images/santri/<?=$foto;?>.JPG" width="70px" hight="105px">
                                                </th>
                                                <td style="font-size: 18px"><?=$notifikasi_pengembalian;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                        <?php } ?>


                        <div class="card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                            <h3 class="text-center">CARI</h3>
                                            <p class="text-center">Peminjaman/Pengembalian Laptop Berdasar No NISN Santri</p>
                                    </div>
                                </div>
                                    <form id="myform" action="<?=base_url();?>home/cariNISN" method="POST">
                                        <div class="input-group input-group-button">
                                            <input type="text" class="form-control" id="nisn" name="nisn" placeholder="0050354582" required autofocus>
                                            <button type="submit" class="input-group-addon btn btn-primary" id="basic-addon10">
                                                <span class=""><i class="icofont icofont-search"></i>Cari</span>
                                            </button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                   


                    <!--<div class="login-card card-block auth-body mr-auto ml-auto">-->
                        <!--<form class="md-float-material form-material">-->
                            <!--<div class="text-center">-->
                                <!--<img src="../files/assets/images/logo.png" alt="logo.png">-->
                            <!--</div>-->
                            <!--<div class="auth-box">-->
                                <!---->
                            <!--</div>-->
                        <!--</form>-->
                        <!--&lt;!&ndash; end of form &ndash;&gt;-->
                    <!--</div>-->
                    <!-- Authentication card end -->
                </div>
                 <div class="col-sm-4"></div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->


        </div>
        <!-- end of container-fluid -->
    </section>

    <div class="footer bg-inverse">
        <p class="text-center">Copyright &copy; 2019 IMMIM PUTRA MAKASSAR, All rights reserved.</p>
    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="../files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery\js\jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\bootstrap\js\bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\modernizr\js\css-scrollbars.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\i18next\js\i18next.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\bower_components\jquery-i18next\js\jquery-i18next.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>files\assets\js\common-pages.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

<!-- <script type="text/javascript"> 
    
    $(document).ready(function(){
      $("input[name=nisn]").bind("change keyup input", function(){
            var nisn = $(this).val();
            if(nisn.length == 10) {
                    $.ajax({
                    type: 'POST',
                    data: {nisn:nisn},
                    cache: false,
                    url: '<?php echo base_url() ?>home/carinisn',
                    success: function (data) {
                        //alert(results);
                        $("#notifikasi").html(data);
                        document.getElementById('myform').reset();
                    }
                });
            }
      });      
    });
</script> -->

</body>

</html>
