<?php

 class Pl_datapinjam extends CI_Model {

  private $table = 'pl_datapinjam';

 public function get($select, $where=NULL, $order=NULL, $limit=NULL){
 		if (!empty($where)) {
			$this->db->where($where);
		}
        if (!empty($order)) {
            $this->db->order_by('tanggalpinjam', $order);
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }   
    	$this->db->select($select);
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query;
    }

 public function getJoin($select, $where_1=NULL, $where_2=NULL){
        if (!empty($where_1)) {
            $this->db->where($where_1);
        }
        if (!empty($where_2)) {
            $this->db->where($where_2);
        }  
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->join('pl_santri', 'pl_santri.idsantri = '.$this->table.'.idsantri');
        $query = $this->db->get();
        return $query;
 }

 public function insert($data){
        $this->db->insert($this->table, $data);
    }
  
 public function update($set, $where){
        $this->db->where($where);
        $this->db->update($this->table, $set);
    }
  
 public function delete($where){
        $this->db->where($where);
        $this->db->delete($this->table);
    }
 }