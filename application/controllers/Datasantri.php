<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasantri extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$username = $this->session->userdata('username');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		//title head
		$data['title']='Data Santri | Pinjam Laptop';
		//Menu
		$data['dashboard_active']='';
		$data['pengembalian_active']='';
		$data['peminjaman_active']='';
		$data['datasantri_active']='active';
		$data['laporan_active']='';
		$data['konfigurasi_active']='';
		$data['profil_active']='';
		//notifikasi
		$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		//Query
		$this->load->model('Pl_santri');
		$this->load->model('Pl_kelas');

		$data['kelas'] = $this->Pl_kelas->get('kelas');
		$data['query'] = $this->Pl_santri->get('*');
		$this->load->view('admin/v_datasantri', $data);
	}

	public function doTambahDataSantri(){
		$this->load->model('Pl_santri');
		$data['nisn'] = $this->input->post('nisn');
		$data['nama'] = $this->input->post('nama');
		$data['ttl'] = $this->input->post('ttl');
		$data['kelas'] = $this->input->post('kelas');
		$data['lembaga'] = $this->input->post('lembaga');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$query = $this->Pl_santri->insert($data);
		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil menambah data santri");
		redirect('datasantri', 'location');
	}

	public function doEditDataSantri(){
		$this->load->model('Pl_santri');
		$data['nisn'] = $this->input->post('nisn');
		$data['nama'] = $this->input->post('nama');
		$data['ttl'] = $this->input->post('ttl');
		$data['kelas'] = $this->input->post('kelas');
		$data['lembaga'] = $this->input->post('lembaga');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$where['idsantri'] = $this->input->post('idsantri');
		$query = $this->Pl_santri->update($data, $where);
		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil menambah data santri");
		redirect('datasantri', 'location');
	}

	public function deleteDataSantri(){
		$this->load->model('Pl_santri');
		$where['idsantri'] = $this->input->post('idsantri');
		$query = $this->Pl_santri->delete($where);
		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil menghapus data santri");
		redirect('datasantri', 'location');
	}

	public function doUploadFoto(){
		$this->load->model('Pl_santri');
		$nisn = $this->input->post('nisn');
		$idsantri = $this->input->post('idsantri');

		$this->load->library('upload');
		//UPLOAD FOTO
		$config['overwrite'] = TRUE;
	    $config['upload_path'] = './files/assets/images/santri/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG'; //type yang dapat diakses bisa anda sesuaikan
	    $config['file_name'] = $nisn; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if ( ! $this->upload->do_upload('fileFoto'))
	    {
	    	//pesan yang muncul jika terdapat error dimasukkan pada session flashdata
	    	$this->session->set_flashdata("notifikasi_gagal", "Upload Foto Gagal ! File Tidak Ada atau Format Tidak Sesuai");
	    	redirect('datasantri', 'location');
	    }
	    else {
	    	$this->upload->data();
	    	$data['foto'] = $nisn;
	    	$where['idsantri'] = $idsantri;
	    	$this->Pl_santri->update($data, $where);
	    	//notifikasi
	    	$this->session->set_flashdata("notifikasi_berhasil", "Upload Foto Berhasil !");
	    	redirect('datasantri', 'location');
	    }
	}


}