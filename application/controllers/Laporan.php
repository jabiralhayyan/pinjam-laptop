<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		$username = $this->session->userdata('username');
		$data['query'] ='';
		//title head
		$data['title']='Laporan Peminjaman | Pinjam Laptop';
		//Notifikasi
		$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		//Menu
		$data['dashboard_active']='';
		$data['pengembalian_active']='';
		$data['peminjaman_active']='';
		$data['datasantri_active']='';
		$data['laporan_active']='active';
		$data['konfigurasi_active']='';
		$data['profil_active']='';
		//Query
		$this->load->model('Pl_datapinjam');
		$this->load->model('Pl_kelas');
		$data['kelas'] = $this->Pl_kelas->get('*');
		$lembaga = $this->input->post('lembaga');
		$kelas = $this->input->post('kelas');
		if($lembaga !='') {
			$where_1['lembaga'] = $lembaga;
			$where_2['kelas'] = $kelas;
			$data['query'] = $this->Pl_datapinjam->getJoin('*', $where_1, $where_2);	
			$data['_lembaga'] = $lembaga;
			$data['_kelas'] = $kelas;		
		}
		$this->load->view('admin/v_laporan', $data);
	}

	public function exportLaporan()
	{
		$this->load->library("PHPExcel");
		//Query
		$this->load->model('Pl_datapinjam');
		$lembaga = $this->input->post('lembaga');
		$kelas = $this->input->post('kelas');
		$where_1['lembaga'] = $lembaga;
		$where_2['kelas'] = $kelas;
		$data['query'] = $this->Pl_datapinjam->getJoin('*', $where_1, $where_2);
		$data['lembaga'] = $lembaga;
		$data['kelas'] = $kelas;
		$this->load->view('admin/cetak/cetaklaporan', $data);		
	}

}