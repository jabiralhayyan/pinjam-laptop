<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		$username = $this->session->userdata('username');
		//title head
		$data['title']='Peminjaman | Pinjam Laptop';
		//Menu
		$data['dashboard_active']='';
		$data['pengembalian_active']='';
		$data['peminjaman_active']='active';
		$data['datasantri_active']='';
		$data['laporan_active']='';
		$data['konfigurasi_active']='';
		$data['profil_active']='';
		//Notifikasi
		$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		//Query
		$data['query'] = '';
		$this->load->model('Pl_santri');
		$nisn = $this->input->post('nisn');
		if($nisn != '') {
			$where['nisn'] = $nisn;
			$query = $this->Pl_santri->get('*', $where);
			if($query->num_rows() > 0 ) {
				foreach ($query->result() as $row) {
					$data['query'] = 'sukses';
					$data['idsantri'] = $row->idsantri;
					$data['nisn'] = $row->nisn;
					$data['nama'] = $row->nama;
					$data['lembaga'] = $row->lembaga;
					$data['kelas'] = $row->kelas;
					$data['ttl'] = $row->ttl;
				}
			}
		}
		
		$this->load->view('admin/v_peminjaman', $data);
	}

	public function doPinjam()
	{
		$this->load->model('Pl_datapinjam');
		//diberikan kondisi jika laptop belum dikembalikan, tidak akan bisa pinjam
		$idstatus = '';
		$idsantri = $this->input->post('idsantri');
		$where['idsantri'] = $idsantri;
		$order = 'DESC';
		$limit = 1;
		$getPeminjaman = $this->Pl_datapinjam->get("*", $where, $order, $limit);
		if($getPeminjaman->num_rows() > 0) {
			foreach ($getPeminjaman->result() as $row) {
				$idstatus = $row->idstatus;
			}
			if($idstatus == '1') {
				$this->session->set_flashdata("notifikasi_gagal", "Maaf ! Santri tidak dapat meminjam karena belum mengembalikan laptop");
				redirect('peminjaman', 'location');
			}
		}
		else {
			$data['idsantri'] = $idsantri;
			$data['tanggalpinjam'] = date('Y-m-d H:i:s');
			$data['idstatus'] = '1';
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$query = $this->Pl_datapinjam->insert($data);
			$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil Meminjamkan Laptop");
			redirect('peminjaman', 'location');
		}

	}



}