<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		$username = $this->session->userdata('username');
		//title head
		$data['title']='Konfigurasi | Pinjam Laptop';
		//Menu
		$data['dashboard_active']='';
		$data['pengembalian_active']='';
		$data['peminjaman_active']='';
		$data['datasantri_active']='';
		$data['laporan_active']='';
		$data['konfigurasi_active']='active';
		$data['profil_active']='';
		//notifikasi
		$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		//query
		$this->load->model('Pl_konfigurasi');
		$query = $this->Pl_konfigurasi->get('*');
		foreach ($query->result() as $row) {
			$data['idkonfigurasi'] = $row->idkonfigurasi;
			$data['namasistem'] = $row->namasistem;
			$data['instansi'] = $row->instansi;
			$data['alamat'] = $row->alamat;
			$data['email'] = $row->email;
			$data['notelepon'] = $row->notelepon;
		}
		$this->load->view('admin/v_konfigurasi', $data);
	}


	public function doEditKonfigurasi()
	{
		$this->load->model('Pl_konfigurasi');
		$data['namasistem'] = $this->input->post('namasistem');
		$data['instansi'] = $this->input->post('instansi');
		$data['alamat'] = $this->input->post('alamat');
		$data['email'] = $this->input->post('email');
		$data['notelepon'] = $this->input->post('notelepon');
		$where['idkonfigurasi'] = $this->input->post('idkonfigurasi');
		$query = $this->Pl_konfigurasi->update($data, $where);
		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil mengubah data konfigurasi");
		redirect('konfigurasi', 'location');
	}




}