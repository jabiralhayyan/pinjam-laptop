<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$login = $this->session->userdata('logged_in');
		$admin = $this->session->userdata('is_admin');
		if ($login==false && $admin==false)
		{
			//title head
			$data['title']='Admin Login | Master Data';
			//pesan berhasil
			$data['logout_berhasil'] = $this->session->flashdata('logout_berhasil');
			//pesan gagal
			$data['username_password_salah'] = $this->session->flashdata('username_password_salah');
			$this->load->view('admin/v_loginadmin', $data);
		}
		else
		{
			$this->load->helper('url');
			redirect('dashboard','location');
		}  
	}

	//fungsi untuk login
	public function login()
	{
		$this->load->model('Pl_admin');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array('username' => $username,
					   'password' => md5($password)
					);
		$query = $this->Pl_admin->get('*', $where);
		if ($query->num_rows()>0)
		{
			foreach ($query->result() as $row)
			{
				$newdata = array(
					'username' => $row->username,
					'namaadmin' => $row->namaadmin,
					'logged_in' => TRUE,
					'is_admin' => TRUE
					);
				$this->session->set_userdata($newdata);
			}
			redirect('admin','location');
		}
		else 
		{
			$this->session->set_flashdata('username_password_salah','Maaf ! Username & Password Anda Salah');
			redirect('admin','location');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('is_admin');	
		$this->session->set_flashdata('logout_berhasil','Anda berhasil logout');
		redirect('admin','refresh');
	}

	public function profil()
	{
		$login = $this->session->userdata('logged_in');
		$admin = $this->session->userdata('is_admin');
		$username = $this->session->userdata('username');
		$namaadmin = $this->session->userdata('namaadmin');

		if ($login==false && $admin==false)
		{
			redirect('admin','location');
		}
		else
		{
			//title head
			$data['title']='Profil Admin | Pinjam Laptop';
			$data['namaadmin'] = $namaadmin;
			//Menu
			$data['dashboard_active']='';
			$data['pengembalian_active']='';
			$data['peminjaman_active']='';
			$data['datasantri_active']='';
			$data['konfigurasi_active']='';
			$data['profil_active']='active';
			//notifikasi
			$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
			$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');

			$this->load->model('Pl_admin');
			$query = $this->Pl_admin->get('*');
			foreach($query->result() as $row) {
				$data['username'] = $row->username;
				$data['password'] = $row->password;
				$data['nama'] = $row->namaadmin;
			}

			$this->load->view('admin/v_profiladmin', $data);
		}
	}

	// public function doTambahProfil()
	// {
	// 	$login = $this->session->userdata('logged_in');
	// 	$admin = $this->session->userdata('is_admin');
	// 	$username = $this->session->userdata('username');

	// 	if ($login==false && $admin==false)
	// 	{
	// 		redirect('admin','location');
	// 	}
	// 	else
	// 	{
	// 		$this->load->model('Pl_admin');
	// 		$where['username'] = $this->input->post('username');
	// 		$checkusername = $this->Pl_admin->get('*', $where);
	// 		if($checkusername->num_rows() > 0) {
	// 			$this->session->set_flashdata("notifikasi_gagal", "Maaf ! Username sudah terpakai");
	// 			redirect('admin/profil', 'location');
	// 		}

	// 		$password = $this->input->post('password');
	// 		$konfirmasi = $this->input->post('konfirmasi');
	// 		if($password != $konfirmasi)
	// 		{
	// 			$this->session->set_flashdata("notifikasi_gagal", "Maaf ! Password & konfirmasi password tidak sama");
	// 			redirect('admin/profil', 'location');
	// 		}
	// 		$data['password'] = md5($password);
	// 		$data['namaadmin'] = $this->input->post('namaadmin');			
	// 		$data['username'] = $this->input->post('username');
	// 		$data['created_at'] = date('Y-m-d H:i:s');
	// 		$data['updated_at'] = date('Y-m-d H:i:s');

	// 		$query = $this->Pl_admin->insert($data);
			
	// 		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil menambah data admin");
	// 		redirect('admin/profil', 'location');
	// 	}
	// }

	public function doUpdateProfil()
	{
		$this->load->library('session');
		$username = $this->session->userdata('username');
		$login = $this->session->userdata('logged_in');
		$admin = $this->session->userdata('is_admin');

		if ($login==false && $admin==false)
		{
			redirect('admin','location');
		}
		else
		{
			$this->load->model('Pl_admin');
			$password = $this->input->post('password');
			$konfirmasi = $this->input->post('konfirmasi');
			if($password != $konfirmasi)
			{
				$this->session->set_flashdata("notifikasi_gagal", "Maaf ! Password & konfirmasi password tidak sama");
				redirect('admin/profil', 'location');
			}
			$data['password'] = md5($password);
			$data['username'] = $this->input->post('username');
			$data['namaadmin'] = $this->input->post('nama');
			
			$where = array('username' => $data['username']);

			$query = $this->Pl_admin->update($data, $where);
			
			$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil mengubah password admin");
			redirect('admin/profil', 'location');
		}
	}


	// public function deleteProfil()
	// {
	// 	$this->load->library('session');
	// 	$username = $this->session->userdata('username');
	// 	$login = $this->session->userdata('logged_in');
	// 	$admin = $this->session->userdata('is_admin');

	// 	if ($login==false && $admin==false)
	// 	{
	// 		redirect('admin','location');
	// 	}
	// 	else
	// 	{
	// 		$this->load->model('Pl_admin');
	// 		$data['username'] = $this->input->post('username');
	// 		$where = array('username' => $data['username']);
	// 		$query = $this->Pl_admin->delete($where);
	// 		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil menghapus data admin");
	// 		redirect('admin/profil', 'location');
	// 	}
	// }

}
