<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$data['title']='Home | Pinjam Laptop';
		$data['query'] = '';
		//notifikasi
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		$data['notifikasi_pinjam'] = $this->session->flashdata('notifikasi_pinjam');
		$data['notifikasi_pengembalian'] = $this->session->flashdata('notifikasi_pengembalian');
		$data['foto'] = $this->session->flashdata('foto');
		$this->load->view('v_home', $data);
	}


	public function cariNISN() {
		//Query
		$this->load->model('Pl_santri');
		$nisn = $this->input->post('nisn');
		$where['nisn'] = $nisn;
		$query = $this->Pl_santri->get('*', $where);
		if($query->num_rows() > 0){
			foreach ($query->result() as $row) {
				$data['query'] = 'sukses';
				$data['idsantri'] = $row->idsantri;
				$data['nisn'] = $row->nisn;
				$data['kelas'] = $row->kelas;
				$data['nama'] = $row->nama;
				$data['lembaga'] = $row->lembaga;
				$data['foto'] = $row->foto;
			}
			$this->doPinjam($data['idsantri'], $data['nama'], $data['nisn'], $data['kelas'], $data['lembaga'], $data['foto']);
		}
		else{
			//notifikasi
			$this->session->set_flashdata("notifikasi_gagal", "Maaf ! NISN Santri tidak ditemukan, Mohon periksa kembali");
			redirect('home', 'refresh');
		}
	}

	protected function doPinjam($idsantri, $nama, $nisn, $kelas, $lembaga, $foto)
	{
		$this->load->model('Pl_datapinjam');
		//diberikan kondisi jika laptop belum dikembalikan, tidak akan bisa pinjam
		$where_idsantri['idsantri'] = $idsantri;
		$order = 'DESC';
		$limit = 1;

		$getPeminjaman = $this->Pl_datapinjam->get("*", $where_idsantri, $order, $limit);
		
		if($getPeminjaman->num_rows() == 0) {
			$datas['idsantri'] = $idsantri;
			$datas['tanggalpinjam'] = date('Y-m-d H:i:s');
			$datas['idstatus'] = '1';
			$datas['created_at'] = date('Y-m-d H:i:s');
			$datas['updated_at'] = date('Y-m-d H:i:s');
			$this->Pl_datapinjam->insert($datas);
			//Notifikasi
			$this->session->set_flashdata("notifikasi_pinjam", '<b>Terima Kasih Telah Meminjam Laptop !</b><br>'.$nama.' ('.$nisn.')<br>kelas '.$kelas.'('.$lembaga.')');
			$this->session->set_flashdata("foto", $foto);
			redirect('home', 'refresh');
		}
		else {
			foreach ($getPeminjaman->result() as $datapinjam) {
				$idstatus = $datapinjam->idstatus;
				$iddatapinjam = $datapinjam->iddatapinjam;
				$tanggalkembali = $datapinjam->tanggalkembali;
			}
			if($idstatus == 1 && ($tanggalkembali=='' || $tanggalkembali==NULL)) {
					//Query mengembalikan laptop
					$this->doPengembalian($iddatapinjam);
					$this->session->set_flashdata("notifikasi_pengembalian", '<b>Terima Kasih Telah Mengembalikan Laptop !</b><br>'.$nama.' ('.$nisn.')<br>kelas '.$kelas.'('.$lembaga.')');
					$this->session->set_flashdata("foto", $foto);
					redirect('home', 'refresh');
				}
				else {
					$datas['idsantri'] = $idsantri;
					$datas['tanggalpinjam'] = date('Y-m-d H:i:s');
					$datas['idstatus'] = '1';
					$datas['created_at'] = date('Y-m-d H:i:s');
					$datas['updated_at'] = date('Y-m-d H:i:s');
					$this->Pl_datapinjam->insert($datas);

					//Notifikasi
					$this->session->set_flashdata("notifikasi_pinjam", '<b>Terima Kasih Telah Meminjam Laptop !</b><br>'.$nama.' ('.$nisn.')<br>kelas '.$kelas.'('.$lembaga.')');
					$this->session->set_flashdata("foto", $foto);
					redirect('home', 'refresh');
				}
		}
	}


	protected function doPengembalian($iddatapinjam){
		$data['tanggalkembali'] = date('Y-m-d H:i:s');
		$data['idstatus'] = '2';
		$data['updated_at'] = date('Y-m-d H:i:s');
		$where['iddatapinjam'] = $iddatapinjam;
		$this->Pl_datapinjam->update($data, $where);
	}	



}