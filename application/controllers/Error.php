<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends CI_Controller {

	public function index()
	{
		$data['title'] = '404 | Halaman tidak tersedia';
		$this->load->view('error_404', $data);  
	}

}