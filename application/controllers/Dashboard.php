<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		$username = $this->session->userdata('username');
		//title head
		$data['title']='Dashboard | Pinjam Laptop';
		//Menu
		$data['dashboard_active']='active';
		$data['pengembalian_active']='';
		$data['peminjaman_active']='';
		$data['datasantri_active']='';
		$data['laporan_active']='';
		$data['konfigurasi_active']='';
		$data['profil_active']='';
		//Query
		$this->load->model('Pl_santri');
		$query_jumlah = $this->Pl_santri->get('*');
		$data['jumlahsantri'] = $query_jumlah->num_rows();

		$where_sma['lembaga'] = 'SMA';
		$query_sma = $this->Pl_santri->get('*', $where_sma);
		$data['jumlahsantrisma'] = $query_sma->num_rows();

		$where_ma['lembaga'] = 'MA';
		$query_ma = $this->Pl_santri->get('*', $where_ma);
		$data['jumlahsantrima'] = $query_ma->num_rows();
		
		//Query Pengembalian
		$this->load->model('Pl_datapinjam');
		$where['idstatus'] = 1;
		$query_pinjam = $this->Pl_datapinjam->getJoin('*', $where);
		$data['jumlahpinjam'] = $query_pinjam->num_rows();

		//Query Grafik Statistik
		$tahun = date('Y');
		$where_bulan1 = 'tanggalpinjam LIKE "%'.$tahun.'-01%"';
		$where_bulan2 = 'tanggalpinjam LIKE "%'.$tahun.'-02%"';
		$where_bulan3 = 'tanggalpinjam LIKE "%'.$tahun.'-03%"';
		$where_bulan4 = 'tanggalpinjam LIKE "%'.$tahun.'-04%"';
		$where_bulan5 = 'tanggalpinjam LIKE "%'.$tahun.'-05%"';
		$where_bulan6 = 'tanggalpinjam LIKE "%'.$tahun.'-06%"';
		$where_bulan7 = 'tanggalpinjam LIKE "%'.$tahun.'-07%"';
		$where_bulan8 = 'tanggalpinjam LIKE "%'.$tahun.'-08%"';
		$where_bulan9 = 'tanggalpinjam LIKE "%'.$tahun.'-09%"';
		$where_bulan10 = 'tanggalpinjam LIKE "%'.$tahun.'-10%"';
		$where_bulan11 = 'tanggalpinjam LIKE "%'.$tahun.'-11%"';
		$where_bulan12 = 'tanggalpinjam LIKE "%'.$tahun.'-12%"';

		$data['pinjam_bulan1'] = $this->Pl_datapinjam->get('*', $where_bulan1)->num_rows();
		$data['pinjam_bulan2'] = $this->Pl_datapinjam->get('*', $where_bulan2)->num_rows();
		$data['pinjam_bulan3'] = $this->Pl_datapinjam->get('*', $where_bulan3)->num_rows();
		$data['pinjam_bulan4'] = $this->Pl_datapinjam->get('*', $where_bulan4)->num_rows();
		$data['pinjam_bulan5'] = $this->Pl_datapinjam->get('*', $where_bulan5)->num_rows();
		$data['pinjam_bulan6'] = $this->Pl_datapinjam->get('*', $where_bulan6)->num_rows();
		$data['pinjam_bulan7'] = $this->Pl_datapinjam->get('*', $where_bulan7)->num_rows();
		$data['pinjam_bulan8'] = $this->Pl_datapinjam->get('*', $where_bulan8)->num_rows();
		$data['pinjam_bulan9'] = $this->Pl_datapinjam->get('*', $where_bulan9)->num_rows();
		$data['pinjam_bulan10'] = $this->Pl_datapinjam->get('*', $where_bulan10)->num_rows();
		$data['pinjam_bulan11'] = $this->Pl_datapinjam->get('*', $where_bulan11)->num_rows();
		$data['pinjam_bulan12'] = $this->Pl_datapinjam->get('*', $where_bulan12)->num_rows();

		$this->load->view('admin/v_dashboard', $data);
	}

}