<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('logged_in')==false && $this->session->userdata('is_admin')==false) {
			redirect('admin','location');
		}
    }

	public function index()
	{
		$this->load->library('session');
		$data['namaadmin']=$this->session->userdata('namaadmin');
		$username = $this->session->userdata('username');
		//title head
		$data['title']='Pengembalian | Pinjam Laptop';
		//Notifikasi
		$data['notifikasi_berhasil'] = $this->session->flashdata('notifikasi_berhasil');
		$data['notifikasi_gagal'] = $this->session->flashdata('notifikasi_gagal');
		//Menu
		$data['dashboard_active']='';
		$data['pengembalian_active']='active';
		$data['peminjaman_active']='';
		$data['datasantri_active']='';
		$data['konfigurasi_active']='';
		$data['profil_active']='';
		//Query
		$this->load->model('Pl_datapinjam');
		$where['idstatus'] = 1;
		$data['query'] = $this->Pl_datapinjam->getJoin('*', $where);
		$this->load->view('admin/v_pengembalian', $data);
	}

	public function doPengembalian(){
		$this->load->model('Pl_datapinjam');
		$data['tanggalkembali'] = date('Y-m-d H:i:s');
		$data['idstatus'] = '2';
		$data['updated_at'] = date('Y-m-d H:i:s');
		$where['iddatapinjam'] = $this->input->post('iddatapinjam');
		$query = $this->Pl_datapinjam->update($data, $where);
		$this->session->set_flashdata("notifikasi_berhasil", "Anda berhasil mengembalikan laptop");
		redirect('pengembalian', 'location');
	}	

}